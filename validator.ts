import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms";

export function ConfirmPasswordValidator(controlName: string, matchingControlName: string) {

  return (formgroup: FormGroup) => {
    const control = formgroup.controls[controlName];
    const matchingControl = formgroup.controls[matchingControlName]

    if (
      matchingControl.errors && !matchingControl.errors['confirmPasswordValidator']
    ) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ confirmPasswordValidator: true })
    } else {
      matchingControl.setErrors(null);
    }
  };
}


export function MaxDaysValidator(controlName: string, matchingControlName: string) {
  return (formgroup: FormGroup) => {

    const control = formgroup.controls[controlName];

    const matchingControl = formgroup.controls[matchingControlName]


    if (
      matchingControl.errors && !matchingControl.errors['maxDaysValidator']
    ) {
      return;
    }
    if (control.value > matchingControl.value) {
      matchingControl.setErrors({ maxDaysValidator: true })
    } else {
      matchingControl.setErrors(null);
    }
  };
}

export function characterOnlyValidator(controlName: string) {
  return (formgroup: FormGroup) => {
  const control = formgroup.controls[controlName];
    const pattern = /^[A-Za-z]+$/;
    if (pattern.test(control.value)) {
      return null;
    }
    return { characterOnly: true };
  }

  // Add more custom validators as needed
}



// export function timeRangeValidator() {
//   return (formgroup: FormGroup) => {
//     console.log('formgroup: ', formgroup);

//     const agency = formgroup.get('agencyAvailbility')?.value;
   
//     if (agency) {
//       for (let i = 0; i < agency.length; i++) {
//         const data = agency[i];

//         if (data.isChecked && data.minTime && data.minTime >= data.maxTime) {
//           formgroup.controls['agencyAvailbility'].setErrors({ invalidTimeRange: true });

//         }
//       }
//     }
//     formgroup.controls['agencyAvailbility'].setErrors(null)
//   }
// }
















