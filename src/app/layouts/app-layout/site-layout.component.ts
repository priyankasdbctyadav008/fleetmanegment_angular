import { Component, ElementRef, HostBinding, HostListener, Renderer2 } from '@angular/core';
import { SideNavService } from './side-nav.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatDrawerMode } from '@angular/material/sidenav';
@Component({
  selector: 'app-site-layout',
  templateUrl: './site-layout.component.html',
  styleUrls: ['./site-layout.component.css'],
})
export class SiteLayoutComponent {
  sidebarState = true;
  sideBar = true;
  drawerMode: MatDrawerMode = 'side';

  constructor(public sidebarService: SideNavService, public breakpointObserver: BreakpointObserver) {
    breakpointObserver.observe([Breakpoints.XSmall, Breakpoints.Small]).subscribe(result => {
      if (result.matches) {
        this.drawerMode = 'over';
      } else {
        console.log('result: ', result);
        this.drawerMode = 'side';
      }
    });
  }


  ngOnInit() {
    this.sidebarService.sidebarStateObservable$
    .subscribe((newState: string) => {
      if (newState == 'open') {
        this.sidebarState = true;
      } else {
        this.sidebarState = false;
      }
    });
  }

}
