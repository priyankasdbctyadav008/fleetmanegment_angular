import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AngularMaterialModule } from 'src/app/material.module';
import { SiteLayoutComponent } from './site-layout.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HelperModule } from 'src/app/helper/helper.module';
import { AuthGuard } from 'src/app/auth.guard';
import { SideNavService } from './side-nav.service';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

const routes: Routes = [
  {
    path: '',
    component: SiteLayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('../../main/dashboard/dashboard.module').then(m => m.DashboardsModule),
        title: "Dashboard-Fleet Management"

      },
      {
        path: 'organization',
        loadChildren: () => import('../../main/organization-management/organization.module').then(m => m.OrganizationModule),
        title: "Organization Management-Fleet Management"
      },
      {
        path: 'agency',
        loadChildren: () => import('../../main/agency-management/agency.module').then(m => m.AgencyModule),
        title: "Agency Management-Fleet Management"
      },
      {
        path: 'vehicle',
        loadChildren: () => import('../../main/vehicle-management/vehicle.module').then(m => m.VehicleModule),
        title: "Vehicle Mangement-Fleet Management"
      },
      {
        path: 'tariff',
        loadChildren: () => import('../../main/tariff-management/tariff.module').then(m => m.TariffModule),
        title: "Tariff Package-Fleet Management"
      },
      {
        path: 'reservations',
        loadChildren: () => import('../../main/reservations-management/reservations.module').then(m => m.ReservationsModule),
        title: "Reservation-Fleet Management"
      },
      {
        path: 'custom',
        loadChildren: () => import('../../main/custom-quotation/custom.module').then(m => m.CustomModule),
        title: "Custom-Quotation-Fleet Management"
      },
      {
        path: 'reports',
        loadChildren: () => import('../../main/reports/report.module').then(m => m.ReportModule),
        title: "Reports-Fleet Management"
      },
      {
        path: 'invoice',
        loadChildren: () => import('../../main/invoice-management/invoice.module').then(m => m.InvoiceModule),
        title: "Invoice-Mangement-Fleet Management"
      },
      {
        path: 'customize',
        loadChildren: () => import('../../main/customize-website/customize.module').then(m => m.CustomizeModule),
        title: "Customize-Website-Fleet Management"
      },
      {
        path: 'setting',
        loadChildren: () => import('../../main/settings/setting.module').then(m => m.SettingModule),
        title: "Reservation Settings-Fleet Management"
      },
      {
        path: 'user',
        loadChildren: () => import('../../main/user-management/user.module').then(m => m.UserModule),
        title: "Dashboard-Fleet Management"
      },
      {
        path: 'subscription',
        loadChildren: () => import('../../main/subscription-management/subscription.module').then(m => m.SubscriptionModule),
        title: "Dashboard-Fleet Management"
      },
      {
        path: 'privacy-policy',
        component: PrivacyPolicyComponent
      },
      {
        path: 'terms-condition',
        component: TermsConditionComponent
      },
      {
        path: '**',
        redirectTo: '/dashboard/overview'

      }
    ]
  }
];

@NgModule({
  imports: [
    FlexLayoutModule,
    CommonModule,
    HelperModule,
    RouterModule.forChild(routes),
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    SiteLayoutComponent, HeaderComponent, FooterComponent, NavBarComponent, PrivacyPolicyComponent, TermsConditionComponent
  ],
  exports: [
    SiteLayoutComponent, HeaderComponent
  ],
  providers: [AuthGuard, SideNavService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SiteLayoutModule {
}
