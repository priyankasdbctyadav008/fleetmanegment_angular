export const DEVNAV = 0;
export const DEVADMINNAV = [
    {
        id: 'dashboard-module',
        title: 'Dashboard',
        translate: 'NAV.DASHBOARD.DASHBOARD_MAIN',
        type: 'item',
        url: 'dashboard/overview',
        icon: 'dashboard_customize',
        display: true,
        isExapanded: true,
        isChildren: false,
    },
    {
        id: 'organization',
        title: 'Organization Management',
        translate: 'NAV.ORGANIZATION.ORGNIZATION_MANAGEMENT',
        type: 'item',
        url: 'organization/list',
        icon: 'corporate_fare',
        display: false,
        isExapanded: false,
    },
    {
        id: 'master',
        title: 'Masters',
        translate: 'NAV.MASTER_MANAGEMENT.MASTERS',
        url: 'vehicle/config',
        type: 'collapse',
        icon: 'settings',
        display: false,
        isExapanded: false,
        isChildren: false,
        children: [
            {
                id: 'Vehicle_config',
                title: 'Vehicle Config',
                translate: 'NAV.MASTER_MANAGEMENT.CHILDREN.VEHICLE_CONFIG',
                url: 'vehicle/config',
                type: 'item',
                display: true,
            },
            {
                id: 'vehicle_config',
                title: 'Subscription Plans Config',
                translate: 'NAV.MASTER_MANAGEMENT.CHILDREN.SUBSCRIPTION_PLAN_CONFIG',
                url: '#',
                type: 'item',
                display: false,
            },
        ]
    },
    {
        id: 'subscription management',
        title: 'Subscription Management',
        translate: 'NAV.SUBSCRIPTION_MANAGEMENT.SUBSCRIPTION_MANAGEMENT',
        url: 'subscription/list',
        type: 'item',
        icon: 'playlist_add_check',
        display: false,
        isExapanded: false,
    },
    {
        id: 'reports',
        title: 'Reports',
        translate: 'NAV.REPORT.REPORTS',
        url: 'Reports',
        type: 'item',
        icon: 'insert_chart',
        display: false,
        isExapanded: false,
    },
    // {
    //     id: 'logout',
    //     title: 'LogOut',
    //     translate: 'NAV.LOG_OUT.LOG_OUT',
    //     url: '/auth/signin',
    //     type: 'item',
    //     icon: 'power_settings_new',
    //     display: false,
    //     isExapanded: false,
    // },
];

export const DEVADMINNAV_NAV = [
    {
        id: 'dashboard-module',
        title: 'Dashboard',
        translate: 'NAV.DASHBOARD.DASHBOARD_MAIN',
        type: 'item',
        url: 'dashboard/overview',
        icon: 'dashboard_customize',
        display: true,
        isExapanded: true,
        isChildren: false,
    },
    {
        id: 'agency-management-module',
        title: 'Agency Management',
        translate: 'NAV.AGENCY_MANAGEMENT.AGENCY',
        type: 'item',
        icon: 'add_location',
        url: 'agency/list',
        display: true,
        isExapanded: false,
        isChildren: false,
    },
    {
        id: 'vehicle',
        title: 'Park',
        translate: 'NAV.PARK_MANAGEMENT.PARK',
        url: 'vehicle/list',
        type: 'item',
        icon: 'local_taxi',
        display: true,
        isExapanded: false,
        isChildren: false,
    },
    {
        id: 'tariff',
        title: 'Tariff Packages',
        translate: 'NAV.TARIFF_MANAGEMENT.TARIFF_PACKAGE',
        type: 'item',
        icon: 'auto_awesome_motion',
        url: 'tariff/list',
        display: true,
        isExapanded: false,
        isChildren: false,
    },
    {
        id: 'reservations',
        title: 'Reservations',
        translate: 'NAV.RESERVATION_MANAGEMENT.RESERVATION',
        type: 'item',
        url: 'reservations/list',
        icon: 'today',
        display: true,
        isExapanded: false,
        isChildren: false,
        // children: [
        //     {
        //         id: 'reservations_list',
        //         title: 'List Of Reservation',
        //         translate: 'NAV.RESERVATION_MANAGEMENT.CHILDREN.RESERVATION_LIST',
        //         url: 'reservations/list',
        //         type: 'item',
        //         // icon: 'list',
        //         display: true,
        //     },
        //     {
        //         id: 'reservation_schedule',
        //         title: 'Reservation Schedule',
        //         translate: 'NAV.RESERVATION_MANAGEMENT.CHILDREN.RESERVATION_SCHEDULE',
        //         url: 'reservations/schedule',
        //         type: 'item',
        //         // icon: 'list',
        //         display: false,
        //     },
        // ]
    },
    {
        id: 'custom',
        title: 'Custom Quotation',
        translate: 'NAV.CUSTOM_QUATATION_MANAGMENT.CUSTOM_QUATATION',
        type: 'item',
        icon: 'edit_square',
        url: 'custom/quotes-list',
        display: true,
        isExapanded: false,
    },
    {
        id: 'reports',
        title: 'Invoice Management',
        translate: 'NAV.INVOICE_MANAGEMENT.INVOICE_MANAGEMENT',
        type: 'item',
        url: 'invoice/list',
        icon: 'insert_chart',
        display: true,
        isExapanded: false,
    },
    {
        id: 'customize',
        title: 'Customize Website',
        translate: 'NAV.CUSTOMIZE_WEB.CUSTOMIZE_WEBSITE',
        url: 'customize/website',
        type: 'item',
        icon: 'important_devices',
        display: true,
        isExapanded: false,
    },
    {
        id: 'subscription management',
        title: 'Subscription',
        translate: 'NAV.SUBSCRIPTION_MANAGEMENT.SUBSCRIPTION_MANAGEMENT',
        url: 'subscription/list',
        type: 'item',
        icon: 'playlist_add_check',
        display: true,
        isExapanded: false,
    },
    {
        id: 'settings',
        title: 'Settings',
        translate: 'NAV.SETTINGS.SETTINGS',
        url: 'setting/reservation',
        type: 'item',
        icon: 'settings',
        display: true,
        isExapanded: false,
    },
]
