import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { DEVADMINNAV_NAV } from '../nav';
import { DEVADMINNAV } from '../nav';
import { SideNavService } from '../side-nav.service';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/services/api/api.service';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
  providers: [TranslatePipe]
})
export class NavBarComponent {

  sidebarState: any;
  sideBar: any;
  showButton: any;
  role: any;
  isExpanded: any = false;
  collapse: any = false;
  devNav: any = DEVADMINNAV_NAV;
  devAdminNav: any = DEVADMINNAV;
  selected: any = {};
  navBar: any;
  isMobile = false;
  activeUrl: any;

  constructor(
    private sidebarService: SideNavService, private breakpointObserver: BreakpointObserver,
    public translateService: TranslateService, private auth: AuthService
  ) {
    this.translateService.setDefaultLang('en');
  }


  ngOnInit() {
    this.breakpointObserver.observe([Breakpoints.Handset]).subscribe(result => {
      this.isMobile = result.matches;
      this.token();
      this.navBar = (this.role === '2') ? this.devNav : this.devAdminNav;
      let url = localStorage.getItem('ActiveLink');
      this.activeUrl = (url === null) ? 'dashboard/overview' : url;
    });


    this.sidebarService.sidebarStateObservable$
      .subscribe((newState: string) => {
        this.sidebarState = newState;
      });
    this.sidebarService.sidebarObservable$
      .subscribe((newState: boolean) => {
        this.sideBar = newState;
      });
  }


  navParentClick(value: any) {
    localStorage.setItem('ActiveLink', value.url);
    this.activeUrl = localStorage.getItem('ActiveLink');
    this.navBar.forEach((item: any) => {
      if (item.id === value.id) {
        if (item.type === 'item') {
          item.isExapanded = true;
        }
        else if (item.type === 'collapse') {
          item.isChildren = true;
          item.isExapanded = !item.isExapanded
        }
      }
      else {
        item.isExapanded = false;
        item.isChildren = false;
      }
    });
  }

  token() {
    let data: any = localStorage.getItem('user');
    if (data) {
      data = JSON.parse(data);
      this.role = data.user.role;
    }
  }

  logout() {
    this.auth.logout();
  }
}
