import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TranslateService, TranslatePipe } from '@ngx-translate/core';
import { AuthService } from 'src/app/services/api/api.service';
import { SideNavService } from '../side-nav.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [TranslatePipe]

})
export class HeaderComponent {
  @Output() sidenav: EventEmitter<any> = new EventEmitter();
  @Output() modalShow = new EventEmitter<boolean>();
  id: any;
  User: any;
  org_name: any;
  selectLang: any;
  show: boolean = true;
  isExpanded: boolean = true;
  modes = ['framed', 'full-width', 'boxed'];
  languages = [
    {
      id: 'en',
      title: 'English',
      sortname: "GB"
    },
    {
      id: 'fr',
      title: 'French',
      sortname: "FR"
    },
  ];
  selectedLanguage:any = 'en'
  currentLang: any;
  constructor(public translate: TranslateService, private auth: AuthService, private sidebarService: SideNavService) {
  }

  ngOnInit() {
    let data: any = localStorage.getItem('user')
    let userData = JSON.parse(data);
    this.User = userData.user;
    this.org_name = userData.organizations_details[0];
    console.log(' this.org_name: ',  this.org_name);
    let signLang = localStorage.getItem('lang');
    this.languages.map((value: any) => {
      if (value.id == signLang) {
        this.selectedLanguage = value.id;
        this.translate.use(value.id);
      }
    })
  }

  switchLanguage(lang: any) {
    this.selectedLanguage = lang.id;
    this.translate.use(lang.id);
    localStorage.setItem('lang',lang.id);
  }

  logout() {
    this.auth.logout();
  }

  toggleSideNav() {
    this.sidebarService.toggle();
  }
}