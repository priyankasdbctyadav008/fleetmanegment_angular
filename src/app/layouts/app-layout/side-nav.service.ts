import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable()
export class SideNavService {
  private sidebarState = 'open';
  private sidebar = true;
  private sidebarStateChanged$ = new BehaviorSubject<string>(this.sidebarState);
  private sidebarChanged$ = new BehaviorSubject<boolean>(this.sidebar);
  public sidebarStateObservable$ = this.sidebarStateChanged$.asObservable();
  public sidebarObservable$ = this.sidebarChanged$.asObservable();

  constructor() {
    this.sidebarStateChanged$.next('open');
    this.sidebarChanged$.next(true);
  }

  toggle() {
    this.sidebarState = this.sidebarState === 'open' ? 'close' : 'open';
    this.sidebarStateChanged$.next(this.sidebarState);
    this.sidebar = this.sidebar === true ? false : true;
    this.sidebarChanged$.next(this.sidebar);
  }
}
