import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { SubscriptionPlanComponent } from './subscription-plan/subscription-plan.component';
import { PrivacyPolicyComponent } from '../app-layout/privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from '../app-layout/terms-condition/terms-condition.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'signin',
        component: SigninComponent,
        title: "Login-Fleet Management"
      },
      {
        path: 'signup',
        component: SignupComponent,
        title: "Register-Fleet Management"
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
        title: "Forgot Password-Fleet Management"
      },
      {
        path: 'verify-email',
        component: VerifyEmailComponent,
        title: "Verify Email-Fleet Management"
      },
      {
        path:'subscription-plan',
        component:SubscriptionPlanComponent,
        title: "Subscription Plan-Fleet Management"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
