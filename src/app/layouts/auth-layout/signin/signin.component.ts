import { Component, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/api/api.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent {
  loginForm: FormGroup;
  submitted: boolean = false;
  showPassword: any;
  password: any
  errorMsg: any;
  disabled: boolean = false;
  durationInSeconds = 1;
  data: any;
  loader = true;

  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(private fb: FormBuilder, public authService: AuthService,
    private cookieService: CookieService, private router: Router, private _snackBar: MatSnackBar, public translate: TranslateService) {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    var element: any = document.getElementsByClassName("column");
    element[0]?.classList.add("sigin_parent");
    let localStorageData = localStorage.getItem('user');
    let localStorageActive = localStorage.getItem('ActiveLink');
    if (localStorageData || localStorageActive) {
      localStorage.removeItem('user');
      localStorage.removeItem('ActiveLink');
    }
  }


  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }

  logIn() {
    let localStorageData = localStorage.getItem('user');
    if (this.loginForm.valid) {
      if (localStorageData == null) {
        this.loginForm.value.email = this.loginForm.value.email.toLowerCase();
        try {
          this.authService.loginUser(this.loginForm.value).then(data => {
            if (data.success === true) {
              if (data.user.Isemail_verified == '0') {
                localStorage.setItem('user', JSON.stringify(data.user));
                localStorage.setItem('userEmail', this.loginForm.value.email);
                this.authService.sendOtp(this.loginForm.value.email).then((data) => {
                  this.openSnackBar('Mail send', 'true');
                  this.router.navigate(['/auth/verify-email']);
                })
              } else {
                this.openSnackBar(data.message, 'true');
                this.router.navigate(['/dashboard/overview']);
              }
            } else {
              console.log('data.message: ', data.message);
              this.openSnackBar(data.message, 'false');
            }
          }, (error) => {
            var element: any = document.getElementsByClassName("mat-mdc-form-field-type-mat-input");
            for (var i = 0; i > element.length; i++) {
              element[i]?.classList.add("error_input_box");
            }
            this.openSnackBar('Incorrect email or password', 'false');
          })
        }
        catch (error: any) {
          this.openSnackBar('Enter correct details', 'false');
        }
      } else {
        this.router.navigate(['/dashboard/overview']);
      }
    }
  }

  toggleVisibility() {
    this.showPassword = !this.showPassword
  }

  resendOtp(email: any) {
    try {
      this.authService.sendOtp(email).then((res) => {
        this.openSnackBar('Resend Otp Please check', 'true');
      }, (error) => {
        this.openSnackBar(error.message, 'false');
      })
    } catch (error) {
      this.openSnackBar(error, 'false');
    }
  }
}
