import { Component } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ConfirmPasswordValidator } from 'validator';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { AuthService } from 'src/app/services/api/api.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  registerForm: any = FormGroup;
  showPassword: any;
  password: any;
  countries: any = [];
  city: any = [];
  state: any = [];
  currency: any = [];
  durationInSeconds = 1;
  loader = true;
  checkboxElement: any;
  emailTaken: any;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private _snackBar: MatSnackBar, public translate: TranslateService) {

    this.registerForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])],
      phone: ['', Validators.compose([Validators.required, Validators.maxLength(18)]),Validators.pattern("^(?=.*?[0-9])")],
      name: ['', Validators.required],
      address: ['', Validators.required],
      postalCode: ['', Validators.required],
      city: ['', Validators.required],
      countryId: ['', Validators.required],
      stateId: ['', Validators.required],
      prefferedCurrencies: ['', Validators.required],
      role: ['2'],
      status: [false, Validators.required],
      LogoUrl: ['null'],
      password: ['', Validators.compose([Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")])],
      confirmPassword: ['', Validators.required]
    },
      {
        validators: ConfirmPasswordValidator('password', 'confirmPassword'),
      } as AbstractControlOptions
    )
  }

  ngOnInit() {
    this.currencyList();
    this.countryList()
    var element: any = document.getElementsByClassName("column");
    element[0]?.classList.remove("sigin_parent");
    setTimeout(() => {
      this.loader = false;
    }, 2000);
  }


  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }


  register() {
    this.checkboxElement = document.getElementsByClassName('status_check');
    if (!this.registerForm.value.status) {
      if (this.checkboxElement) {
        this.checkboxElement[0].classList.add('mat-checkbox-color');
      }
    }
    if (this.registerForm.valid) {
      let data = this.registerForm.value
      const preferredCurrenciesJson = JSON.stringify(this.registerForm.value.prefferedCurrencies);
      data['prefferedCurrencies'] = preferredCurrenciesJson;
      data.email = this.registerForm.value.email.toLowerCase();
      try {
        this.authService.registerUser(data)
          .then((res: any) => {
            localStorage.setItem('userEmail', data.email);
            localStorage.setItem('userName', data.first_name);
            localStorage.setItem('user', JSON.stringify(res));
            if (res.success === true) {
              this.openSnackBar('User Registered Successfully ', 'true')
              this.authService.sendOtp(data.email).then((data) => {
                this.registerForm.reset();
                this.router.navigate(['/auth/verify-email']);
                this.openSnackBar('A message has been sent with an OTP to your email.', 'true');
              })
            } else {
              this.emailTaken = res.message.email;
              this.openSnackBar(res.message.email, 'false');
            }
          }, (error) => {
            this.openSnackBar('Internal Server Error', 'false');
          })
      } catch (error) {
        this.openSnackBar(error, 'false');
      }
    }
  }


  async currencyList() {
    try {
      this.authService.currencyList()
        .then((res: any) => {
          this.currency = res.data
        }, (error) => {
          console.error('error: ', error);
        })
    } catch (error) {
      console.error(error);
    }
  }

  updateInputWidth(selectedCountryCode: any) {
    console.log('selectedCountryCode: ', selectedCountryCode);
    // const width = countryWidthMapping[selectedCountryCode] || '150px'; // Default width if not found
    // const inputElement = document.getElementById('yourInputFieldId'); // Replace with your actual input field ID
    // if (inputElement) {
    //   inputElement.style.width = width;
    // }
  }


  async onchange(value: any) {
    try {
      this.stateList(value)
    } catch (error) {
      console.error(error);
    }
  }


  async stateList(value: any) {
    try {
      this.authService.stateList(value)
        .then((res: any) => {
          this.state = res.data
        }, (error) => {
          console.error('error: ', error);
        })
    } catch (error) {
      console.error(error);
    }
  }

  async onChangeCity(value: any) {
    try {
      this.cityList(value);
    } catch (error) {
      console.error(error);
    }
  }
  
  async countryList() {
    try {
      this.authService.countryList()
        .then((res: any) => {
          this.countries = res.data;
        }, (error) => {
          console.error('error: ', error);
        })
    } catch (error) {
      console.error(error);
    }
  }

  cityList(value: any) {
    try {
      this.authService.cityList(value).then(
        (res: any) => {
          this.city = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  public showVisibility() {
    this.showPassword = !this.showPassword
  }

  showpassword() {
    this.password = !this.password;
  }

}
