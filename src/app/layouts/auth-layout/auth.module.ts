import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/signin.component';
import { AuthComponent } from './auth.component';
import { RouterModule } from '@angular/router';
import { PagesRoutingModule } from './pages-routing.module';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AngularMaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgOtpInputModule } from 'ng-otp-input';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { SubscriptionPlanComponent } from './subscription-plan/subscription-plan.component';
import { NgxMatIntlTelInputComponent } from 'ngx-mat-intl-tel-input';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    PagesRoutingModule,
    AngularMaterialModule,
    FormsModule,
    NgOtpInputModule,
    ReactiveFormsModule,
    NgxMatIntlTelInputComponent,
    HttpClientModule, FlexLayoutModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    SigninComponent,
    AuthComponent,
    SignupComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    SubscriptionPlanComponent,
  ],
  exports: [
    AuthComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthModule {
}
