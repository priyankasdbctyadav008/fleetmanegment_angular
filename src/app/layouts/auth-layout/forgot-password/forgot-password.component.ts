import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/api/api.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import {
  MatSnackBar, MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  forgotPasswordForm: FormGroup;
  errorMsg: any;
  disabled: boolean = false;
  durationInSeconds = 1;
  data: any;

  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(private fb: FormBuilder, public authService: AuthService, private router: Router, private _snackBar: MatSnackBar) {
    this.forgotPasswordForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])],
    })
  }

  ngOnInit() {

  }
  
  goBack() {
    window.history.back();
  }

  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }

  forgot() {
    try {
      if (this.forgotPasswordForm.valid) {
        this.authService.forgotPassowrd(this.forgotPasswordForm.value).then((res: any) => {
          if (res.success) {
            this.openSnackBar(res.message, 'true');
            this.router.navigate(['/auth/signin']);
          } else if (res.success === false) {
            this.openSnackBar(res.message, 'false')
          }
        }, (error) => {
          this.openSnackBar('Internal Server Error', 'false');
        });
      } else {
        this.openSnackBar('Email is required', 'false')
      }
    }
    catch (error: any) {
      this.openSnackBar('Internal Server Error', 'false');
    }
  }
}
