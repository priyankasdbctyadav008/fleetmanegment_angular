import { Component, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/api/api.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { TranslateService } from '@ngx-translate/core';
import { NgOtpInputComponent } from 'ng-otp-input';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent {
  @ViewChild(NgOtpInputComponent, { static: false }) ngOtpInput!: NgOtpInputComponent;
  email = localStorage.getItem('userEmail');
  name = localStorage.getItem('userName')
  User: any;
  durationInSeconds = 1;

  @ViewChild('dialogRef') dialogRef!: TemplateRef<any>;
  constructor(public authService: AuthService, private router: Router, private dialog: MatDialog,
    private _snackBar: MatSnackBar, public translate: TranslateService, private fb: FormBuilder) {
  }

  ngOnInit() {
    var element: any = document.getElementsByClassName("column");
    element[0]?.classList.add("sigin_parent");
    let data: any = localStorage.getItem('user');
    this.User = JSON.parse(data);
    this.User = this.User.user;
  }

  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',

    })
  }

  onOtpChange(value: any) {
    try {
      let data = value.split('')
      if (data.length === 4) {
        this.ngOtpInput.otpForm.disable();
        this.authService.verifyEmailOtp(this.email, value)
          .then((res: any) => {
            if (res.success === true) {
              this.openDialog()
              this.router.navigate(['/dashboard/overview']);
            } else if (res.success === false) {
              this.openSnackBar('Invalid OTP', 'false');
              this.ngOtpInput.setValue('');
              this.ngOtpInput.otpForm.enable();
              data = null;
            }
          }, (error) => {
            this.openSnackBar('Something Went Wrong', 'false');
          })
      }
    } catch (error) {
      console.error(error);
    }
  }

  resendOtp() {
    try {
      this.authService.sendOtp(this.email).then((data) => {
        this.openSnackBar('OTP send successfully', 'true');
      }, (error) => {
        this.openSnackBar('Something Went Wrong', 'false');
      })
    } catch (error) {
      console.error(error);
    }
  }


  openDialog() {
    const timeout = 2000;
    const myTempDialog = this.dialog.open(this.dialogRef, { data: '', height: '283px', width: '321px' });
    myTempDialog.afterOpened().subscribe(_ => {
      setTimeout(() => {
        myTempDialog.close();
      }, timeout)
    })
  }

}
