import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {
  settings: any;
  modes = ['framed', 'full-width', 'boxed'];
  initialLanguage = 'en';
  loader = true;
  languages = [
    {
      id: 'en',
      title: 'English',
      sortname: "GB"

    },
    {
      id: 'fr',
      title: 'French',
      sortname: "FR"
    },
  ];
  selectedLanguage: any = this.languages[0];
  currentLang: any;

  constructor(public translate: TranslateService, public httpService: HttpService) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang(this.initialLanguage);
    localStorage.setItem('lang', this.initialLanguage)
  }

  ngOnInit() {
    setTimeout(() => {
      this.loader = false;
    }, 2000);
  }

  switchLanguage(lang: any) {
    this.selectedLanguage = lang.id
    this.translate.use(lang.id);
    console.log('this.translate.use: ', this.translate);
    localStorage.setItem('lang', lang.id)
  }
}
