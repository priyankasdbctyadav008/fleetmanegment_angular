import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { AuthService } from 'src/app/services/api/api.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [TranslatePipe]
})

export class UserProfileComponent {
  registerForm: any = FormGroup;
  showPassword: any;
  password: any;
  countries: any = [];
  state: any = [];
  city:any = [];
  currency: any = [];
  durationInSeconds = 1;
  id: any;
  data: any;
  jsonCurrency: any;

 constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private _snackBar: MatSnackBar, public translate: TranslateService, private route: ActivatedRoute, private location: Location, public translatePipe: TranslatePipe) {
    this.registerForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])],
      phone: ['', Validators.compose([Validators.required, Validators.maxLength(18)])],
      name: ['', Validators.required],
      address: ['', Validators.required],
      postalCode: ['', Validators.required],
      city: ['', Validators.required],
      countryId: ['', Validators.required],
      stateId: ['', Validators.required],
      prefferedCurrencies: ['', Validators.required],
      role: ['2'],
      status: [false, Validators.required],
      LogoUrl: ['null'],
      userId: [''],
      themeColor: ["null"],
      pageContent_TnC: ["null"],
      pageContent_PrivacyPolicy: ["null"],
      domain: [''],
    })
  }

  ngOnInit() {
    this.currencyList();
    this.countryList();
    this.id = this.route.snapshot.params['id'];
    if (this.id) {
      this.organizationGetById(this.id);
    }
  }

  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }

  async organizationGetById(id: any) {
    try {
      await this.authService.organizationGetById(id).then(
        (res: any) => {
          let response = {
            ...res.data.organization,
            ...res.data.user
          }
          const userFromValue = this.registerForm.value;
          this.stateList(response.countryId);
          this.onChangeCity(response.stateId);
          Object.keys(userFromValue).forEach((key) => {
            try {
              if (key === 'prefferedCurrencies') {
                let prefferedcur = JSON.parse(response.prefferedCurrencies);
                let curid = prefferedcur.id;
                this.registerForm.controls[key].setValue(curid);
                this.onChangeCur(curid);
              }
              else if (key === 'email') {
                this.registerForm.controls[key].setValue(response[key]);
                this.registerForm.controls[key].disable();
              }
              else {
                this.registerForm.controls[key].setValue(response[key]);
              }
            } catch (error) {
              console.error(' failed at ' + key + ' ' + error);
            }
          });
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  async updateOrganization() {
    if (this.registerForm.valid) {
      try {
        let data = JSON.parse(JSON.stringify(this.registerForm.value));
        data.prefferedCurrencies = JSON.stringify(this.jsonCurrency);
        if (data.status) {
          data.status = '1';
        } else {
          data.status = '0';
        }
        await this.authService.updateOrgUser(data, this.id).then((res: any) => {
            if (res.success === true) {
              this.openSnackBar(this.translatePipe.transform('CONFIRMATION_MASSAGE.ORG_UPDATE'), 'true')
              this.back();
            } else if (res.success === false) {
              if (res.message && typeof res.message === 'object') {
                const errorMessage = Object.values(res.message)[0];
                this.openSnackBar(errorMessage, 'false')
              }
            }
          }, (error: any) => {
            console.log('error: ', error);

          });
      } catch (error) {
        console.log('catch error: ', error);
      }
    }
  }

  back() {
    this.location.back();
  }

  async currencyList() {
    try {
      await this.authService.currencyList().then(
        (res: any) => {
          this.currency = res.data;
        },
        (error) => {
          console.log('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  onChangeCur(value: any) {
    console.log('value:5656565656565 ', value);
    this.currency.map((curid: any) => {
      if (curid.id === value) {
        this.jsonCurrency = curid;
        this.registerForm.value.prefferedCurrencies = JSON.stringify(this.jsonCurrency);
      }
    });
  }


  onChangeState(value: any) {
    try {
      this.stateList(value);
    } catch (error) {
      console.error(error);
    }
  }

  onChangeCity(value: any) {
    try {
      this.cityList(value);
    } catch (error) {
      console.error(error);
    }
  }

  
  async cityList(value: any) {
    try {
      await this.authService.cityList(value).then(
        (res: any) => {
          this.city = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  async stateList(value: any) {
    console.log('value: ', value);
    try {
      await this.authService.stateList(value).then(
        (res: any) => {
          console.log('Response:111 ', res);
          this.state = res.data;
        },
        (error) => {
          console.log('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  async countryList() {
    try {
      await this.authService.countryList().then(
        (res: any) => {
          console.log('Response:2211', res);
          this.countries = res.data;
        },
        (error) => {
          console.log('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  public showVisibility() {
    this.showPassword = !this.showPassword;
  }

  showpassword() {
    this.password = !this.password;
  }
}