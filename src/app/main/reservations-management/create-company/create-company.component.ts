import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.css']
})
export class CreateCompanyComponent {
  createCompanyForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.createCompanyForm = this.fb.group({
      company_name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      SIREN_number: ['', Validators.required],
      phone_number: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      address: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      zip_code: ['', Validators.required],
      city: ['', Validators.required],
    })
  }
}
