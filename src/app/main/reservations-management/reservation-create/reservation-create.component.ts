import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reservation-create',
  templateUrl: './reservation-create.component.html',
  styleUrls: ['./reservation-create.component.css']
})
export class ReservationCreateComponent {
  isLinear = false;
  rentalFormGroup:FormGroup;
  customerFormGroup:FormGroup;
  companyFormGroup!:FormGroup;
  paymentFormGroup!:FormGroup;

  showCustomerContent = true;
  showCompanyContent = true;
  constructor(private fb:FormBuilder){
  this.rentalFormGroup = this.fb.group({
    Start_Date: ['', Validators.required],
    End_Date: ['', Validators.required],
    Source: ['', Validators.required],
    Status: ['', Validators.required],
    Vehicle_Category: ['', Validators.required],
    Vehicle_Type: ['', Validators.required],
    Select_vehicle:['',Validators.required],
    Amount: ['', Validators.required],
    Pickup:['',Validators.required],
    Dropoff:['',Validators.required]

    });
    this.customerFormGroup = this.fb.group({
      find_customer: ['', Validators.required],
    });
 

    this.companyFormGroup=this.fb.group({
      find_company:['',Validators.required]
    })

this.paymentFormGroup=this.fb.group({
  Choice:['',Validators.required],
  Agency:['',Validators.required],
  Payment:[''],
  Info:[''],
})
  }
}
