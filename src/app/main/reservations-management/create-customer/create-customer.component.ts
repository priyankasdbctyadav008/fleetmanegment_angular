import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerComponent {
  createCustomerForm!: FormGroup;

  constructor(private fb: FormBuilder) {
    this.createCustomerForm = this.fb.group({
      Name: ['', Validators.required],
      LastName: ['', Validators.required],
      Email: ['', Validators.compose([Validators.required, Validators.email])],
      PhoneNumber: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      Licence: ['', Validators.required],
      DOB: ['', Validators.required],
      Dateoflicence: ['', Validators.required],
      Address: ['', Validators.required],
      Country: ['', Validators.required],
      State: ['', Validators.required],
      ZipCode: ['', Validators.required],
      City: ['', Validators.required],
    })
  }


 
  ngOnInit(): void {
   
  }

  

}
