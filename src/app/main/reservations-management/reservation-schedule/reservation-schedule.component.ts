import { Component, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/core';
import timeGridPlugin from '@fullcalendar/timegrid'


import dayGridPlugin from '@fullcalendar/daygrid';


@Component({
  selector: 'app-reservation-schedule',
  templateUrl: './reservation-schedule.component.html',
  styleUrls: ['./reservation-schedule.component.css']
})
export class ReservationScheduleComponent implements OnInit {

 
  calendarOptions: CalendarOptions = {


  plugins: [dayGridPlugin,timeGridPlugin],
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay',
    },
    // events: [
    //    { title: 'Meeting', start: new Date() }
    //    ]

   
  };
 
constructor(){

}
ngOnInit(): void {
 
}

}
