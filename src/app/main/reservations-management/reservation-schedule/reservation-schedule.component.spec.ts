import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationScheduleComponent } from './reservation-schedule.component';

describe('ReservationScheduleComponent', () => {
  let component: ReservationScheduleComponent;
  let fixture: ComponentFixture<ReservationScheduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservationScheduleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReservationScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
