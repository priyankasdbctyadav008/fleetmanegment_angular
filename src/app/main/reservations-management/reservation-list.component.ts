import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {
  @ViewChild('paginator') paginator!: MatPaginator;
  searchTerm: any;
  DATA = [
    { Source: 'Online', Vehicle: 'Toyota Yaris Cross ', Customer: 'test1', Start_Date: '28/03/2023', End_Date: '29/04/2023', Amount: 'DT 20.00', actions: '' },
    { Source: 'Admin', Vehicle: 'Yaris Cross ', Customer: 'test123', Start_Date: '29/03/2023', End_Date: '01/04/2023', Amount: 'DT 20.00', actions: '' },

  ];
  dataSource!: MatTableDataSource<any>;
  columnsToDisplay = ['Source', 'Vehicle', 'Customer', 'Start_Date', 'End_Date', 'Amount', 'actions'];

  constructor(private cdr: ChangeDetectorRef) { }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.DATA);
  }

  search() {

  }
}
