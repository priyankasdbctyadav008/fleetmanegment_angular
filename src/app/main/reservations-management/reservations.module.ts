import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReservationListComponent } from './reservation-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/material.module';
import { ReservationCreateComponent } from './reservation-create/reservation-create.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { CreateCompanyComponent } from './create-company/create-company.component';
import { ReservationScheduleComponent } from './reservation-schedule/reservation-schedule.component';

import { FullCalendarModule } from '@fullcalendar/angular';

const routes: Routes = [
  {
    path: 'overview',
    component: ReservationListComponent,
  },
  {
    path: 'create',
    component: ReservationCreateComponent
  },
  {
    path:'list',
    component:ReservationListComponent
  },
  {
    path:'schedule',
    component:ReservationScheduleComponent
  }
];

@NgModule({
  declarations: [ReservationListComponent,ReservationCreateComponent,ReservationListComponent,ReservationScheduleComponent, CreateCustomerComponent, CreateCompanyComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FullCalendarModule,
    AngularMaterialModule,
    FlexLayoutModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    ReservationListComponent,ReservationCreateComponent,CreateCustomerComponent,CreateCompanyComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReservationsModule { }
