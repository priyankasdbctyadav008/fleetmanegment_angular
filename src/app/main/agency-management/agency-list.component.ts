import { BreakpointObserver } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/api/api.service';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { HttpService } from 'src/app/services/http.service';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ViewDetailsPopupComponent } from 'src/app/helper/view-details-popup/view-details-popup.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-agency-list',
  templateUrl: './agency-list.component.html',
  styleUrls: ['./agency-list.component.css'],
  providers: [TranslatePipe]
})


export class AgencyListComponent {
  Agencies: any = []
  durationInSeconds = 1;
  Org: any;
  constructor(private breakpointObserver: BreakpointObserver, private authService: AuthService,
    private router: Router, public translateService: TranslateService, public httpService: HttpService,
    private _snackBar: MatSnackBar,public dialog: MatDialog, 
    public _translatePipe: TranslatePipe) {
    this.translateService.setDefaultLang('en');
    let data: any = localStorage.getItem('user')
    let userData = JSON.parse(data);
    this.Org = userData.organizations_details[0];
  }

  ngOnInit() {
    this.getList();
  }

  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }


  async getList() {
    try {
      this.authService.agencyListById(this.Org.id).then((res: any) => {
        this.Agencies = res.data;
        if (this.Agencies) {
          for (let i = 0; i <= this.Agencies.length; i++) {
            if (this.Agencies[i]) {
              this.Agencies[i].agencyAvailbility = JSON.parse(this.Agencies[i]?.agencyAvailbility);
              this.Agencies[i]['agencyTrue'] = this.Agencies[i].agencyAvailbility.find((item: any) => item.isChecked === true)
            }
          }
          console.log('this.Agencies', this.Agencies);
        }
      })
    } catch (error) {

    }
  }


  async getListById() {
    try {
      this.authService.agencyListById('').then((res: any) => {
      })
    } catch (error) {

    }
  }


  async deleteAgency(id: any, i: any) {
    try {
      this.authService.deleteAgency(id).then((res: any) => {
        if (res.success === true) {
          this.openSnackBar(res.message, 'true');
        }
        else if (res.success === false) {
          this.openSnackBar(res.message, 'false')
        }
        this.getList()
      })
    } catch (error) {
      this.openSnackBar(error, 'false')
    }
  }

  openDialog(dialogData: any): void {
    const dialogRef = this.dialog.open(ViewDetailsPopupComponent, {
      data: dialogData,
      height: '607px',
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log('The dialog was closed');
    });
  }
}
