import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { Location } from '@angular/common'
import { AuthService } from 'src/app/services/api/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-agency-create',
  templateUrl: './agency-create.component.html',
  styleUrls: ['./agency-create.component.css'],
  providers: [TranslatePipe]
})
export class AgencyCreateComponent {
  isDisabled: boolean = false;
  addAgencyForm: FormGroup;
  time: any;
  Countries: any = []
  State: any = [];
  isFormFieldEnabled = false;
  durationInSeconds = 1;
  id: any;
  viewId: any;
  isAddMode: any;
  User: any;
  Org: any;
  city: any;
  invalidTimeRangeExists = false;
  checkBoxValidity: boolean = false;
  agencyAvailbility =
    [
      { value: "Monday", isChecked: false, minTime: "00:00", maxTime: "23:59", translate: 'AGENCY.MONDAY', range: '' },
      { value: "Tuesday", isChecked: false, minTime: "00:00", maxTime: "23:59", translate: 'AGENCY.TUESDAY', range: '' },
      { value: "Wednesday", isChecked: false, minTime: "00:00", maxTime: "23:59", translate: 'AGENCY.WEDNESDAY', range: '' },
      { value: "Thursday", isChecked: false, minTime: "00:00", maxTime: "23:59", translate: 'AGENCY.THURSDAY', range: '' },
      { value: "Friday", isChecked: false, minTime: "00:00", maxTime: "23:59", translate: 'AGENCY.FRIDAY', range: '' },
      { value: "Saturday", isChecked: false, minTime: "00:00", maxTime: "23:59", translate: 'AGENCY.SATURDAY', range: '' },
      { value: "Sunday", isChecked: false, minTime: "00:00", maxTime: "23:59", translate: 'AGENCY.SUNDAY', range: '' }
    ];

  constructor(private fb: FormBuilder, private location: Location, private authService: AuthService, private router: Router, private _snackBar: MatSnackBar, private route: ActivatedRoute, public translate: TranslateService,
    public translatePipe: TranslatePipe) {
    let data: any = localStorage.getItem('user')
    let userData = JSON.parse(data);
    this.Org = userData.organizations_details[0];

    this.addAgencyForm = this.fb.group({
      name: ['', Validators.required],
      noOfRegisteredVehicles: ['', Validators.required],
      agencyCode: ['', Validators.required],
      postalCode: ['', Validators.required],
      address1: ['', Validators.required],
      city: ['', Validators.required],
      agencyAvailbility: [, [this.atLeastOneCheckedValidator.bind(this), this.updateTimeRangeValidity.bind(this)]],
      countryId: ['', Validators.required],
      stateId: ['', Validators.required],
      status: [false, Validators.required],
      paymentGatewaySecretKey: [''],
      paymentGatewayPublickey: [''],
      minTime: [],
      maxTime: [],
      ispaymentgatewayEnabled: [''],
      organizationId: [this.Org.id]
    });

  }


  ngOnInit() {
    console.log('this.route: ', this.route.snapshot.routeConfig?.path);
    this.id = this.route.snapshot.params['id'];
    this.viewId = this.route.snapshot.params['viewId'];
    if (this.id) {
      this.getAgencyById(this.id);
    } else if (this.viewId) {
      this.getAgencyById(this.viewId);
    }
    this.countryList();
    this.addAgencyForm.controls['agencyAvailbility'].setValue(this.agencyAvailbility);

  }


  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }



  updateTimeRangeValidity() {
    const agency: any[] = this.addAgencyForm?.controls['agencyAvailbility'].value;
    if (agency) {
      let hasInvalidTimeRanges = false;
      for (let i = 0; i < agency.length; i++) {
        const value = agency[i];
        if (value.isChecked && value.minTime >= value.maxTime) {
          value.range = `Invalid time range`;
          hasInvalidTimeRanges = true;
        } else {
          value.range = '';
        }
      }
      if (hasInvalidTimeRanges) {
        return { invalidTimeRange: true };
      } else {
        return null;
      }
    } else {
      return { invalidTimeRange: true };
    }
  }


  atLeastOneCheckedValidator() {
    const agency: any[] = this.addAgencyForm?.controls['agencyAvailbility'].value;
    if (agency) {
      const isChecked = agency.filter((day: any) => day.isChecked);
      if (isChecked.length > 0) {
        return false;
      } else {
        return { atLeastOneCheckedError: true };
      }
    } else {
      return { atLeastOneCheckedError: true };
    }
  }


  onChange(index: any, bool: boolean) {
    this.addAgencyForm.value.agencyAvailbility[index].isChecked = !this.addAgencyForm.value.agencyAvailbility[index].isChecked;
    this.addAgencyForm.controls['agencyAvailbility'].setValue(this.addAgencyForm.controls['agencyAvailbility'].value);
    this.checkBoxValidity = !this.addAgencyForm.value.agencyAvailbility[index].isChecked;
    if (!this.addAgencyForm.value.agencyAvailbility[index].isChecked) {
      this.addAgencyForm.value.agencyAvailbility[index].minTime = "00:00";
      this.addAgencyForm.value.agencyAvailbility[index].maxTime = "23:59";
    }
  }

  onChangeMin(data: any, index: any, event: any) {
    this.agencyAvailbility.map((days: any) => {
      if (days.value === data.value) {
        data.minTime = event.target.value;
        this.addAgencyForm.controls['agencyAvailbility'].updateValueAndValidity();
      }
    });
  }


  onChangeMax(data: any, index: any, event: any) {
    this.agencyAvailbility.map((days: any) => {
      if (days.value === data.value) {
        data.maxTime = event.target.value;
        this.addAgencyForm.controls['agencyAvailbility'].updateValueAndValidity();
      }
    })
  }


  back() {
    this.location.back();
  }

  async countryList() {
    try {
      this.authService.countryList()
        .then((res: any) => {
          this.Countries = res.data;
        }, (error) => {
          console.error('error', error);
        })
    } catch (error) {
      console.error(error);
    }
  }

  async stateList(value: any) {
    try {
      this.authService.stateList(value)
        .then((res: any) => {
          this.State = res.data;
        }, (error) => {
          console.error('error: ', error);
        })
    } catch (error) {
      console.error(error);
    }
  }

  async onChangeState(value: any) {
    try {
      this.stateList(value)
    } catch (error) {
      console.error(error);
    }
  }

  async onChangeCity(value: any) {
    try {
      this.cityList(value);
    } catch (error) {
      console.error(error);
    }
  }

  cityList(value: any) {
    try {
      this.authService.cityList(value).then(
        (res: any) => {
          this.city = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  enableReq(event: any) {
    this.addAgencyForm.controls['ispaymentgatewayEnabled'];
    if (this.addAgencyForm.controls['ispaymentgatewayEnabled'].value) {
      this.addAgencyForm.controls['paymentGatewayPublickey'].addValidators(Validators.required);
      this.addAgencyForm.controls['paymentGatewaySecretKey'].addValidators(Validators.required);
    } else {
      this.addAgencyForm.controls['paymentGatewayPublickey'].setValue('');
      this.addAgencyForm.controls['paymentGatewaySecretKey'].setValue('');
      this.addAgencyForm.controls['paymentGatewayPublickey'].removeValidators(Validators.required);
      this.addAgencyForm.controls['paymentGatewaySecretKey'].removeValidators(Validators.required);
    }
    this.addAgencyForm.controls['paymentGatewayPublickey'].updateValueAndValidity();
    this.addAgencyForm.controls['paymentGatewaySecretKey'].updateValueAndValidity();
  }


  submit() {
    console.log(this.addAgencyForm.controls)
    let data: any = this.atLeastOneCheckedValidator()
    this.checkBoxValidity = data.atLeastOneCheckedError;
    if (this.id) {
      this.updateUser();
    } else {
      if (this.addAgencyForm.valid) {
        try {
          let data = JSON.parse(JSON.stringify(this.addAgencyForm.value));
          data.agencyAvailbility = JSON.stringify(data.agencyAvailbility);
          if (data.status) {
            data.status = '1';
          } else {
            data.status = '0';
          }
          if (data.ispaymentgatewayEnabled) {
            data.ispaymentgatewayEnabled = '1'
          } else {
            data.ispaymentgatewayEnabled = '0'
          }
          this.authService.agencyCreate(data).then((res: any) => {
            if (res.success === true) {
              this.openSnackBar(res.message, 'true');
              this.addAgencyForm.reset();
              this.router.navigate(['/agency/list']);
            }
          }, (error) => {
            this.openSnackBar(error, 'false');
          })

        } catch (error) {
          this.openSnackBar(error, 'false');
        }
      }
    }
  }


  getAgencyById(value: any) {
    try {
      this.authService.getAgencyById(value).then((res: any) => {
        let response = res.data.agencies;
        const userFromValue = this.addAgencyForm.value;
        this.stateList(res.data.agencies.countryId);
        this.cityList(res.data.agencies.stateId);
        Object.keys(userFromValue).forEach(key => {
          try {
            if (key === 'agencyAvailbility') {
              this.addAgencyForm.controls[key].setValue(JSON.parse(response.agencyAvailbility));
            }
            else if (key === 'status') {
              if (response.status == '0') {
                this.addAgencyForm.controls[key].setValue(false); // Use setValue to update the form control's value
              } else {
                this.addAgencyForm.controls[key].setValue(true);  // Use setValue to update the form control's value
              }
            }
            else {
              this.addAgencyForm.controls[key].setValue(response[key]);
            }
          } catch (error) {
            console.error(' failed at ' + key + ' ' + error);
          }
        });
      }, (error) => {
        console.error('error: ', error);
      })
    } catch (error) {
      console.error(error)
    }
  }


  updateUser() {
    this.enableReq(this.addAgencyForm)
    if (this.addAgencyForm.valid) {
      try {
        let data = JSON.parse(JSON.stringify(this.addAgencyForm.value));
        data.agencyAvailbility = JSON.stringify(data.agencyAvailbility);
        if (data.status) {
          data.status = '1';
        } else {
          data.status = '0';
        }
        if (data.ispaymentgatewayEnabled) {
          data.ispaymentgatewayEnabled = '1'
        } else {
          data.ispaymentgatewayEnabled = '0'
        }
        this.authService.agencyUpdate(data, this.id).then((res: any) => {
          if (res.success === true) {
            this.openSnackBar(res.message, 'true');
            this.router.navigate(['/agency/list'])
          } else if (res.success === false) {
            if (res.message && typeof res.message === 'object') {
              const errorMessage = Object.values(res.message)[0];
              this.openSnackBar(errorMessage, 'false')
            }
          }
        }, (error) => {
          this.openSnackBar(error, 'false');

        })
      } catch (error) {
        this.openSnackBar(error, 'false')
      }
    }
  }
}
