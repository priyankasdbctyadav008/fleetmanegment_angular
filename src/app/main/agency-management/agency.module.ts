import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AngularMaterialModule } from 'src/app/material.module';
import { AgencyCreateComponent } from './agency-create/agency-create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgencyListComponent } from './agency-list.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}
const routes: Routes = [
  {
    path: 'add',
    component: AgencyCreateComponent,
  },
  {
    path: 'list',
    component: AgencyListComponent,
  },
  {
    path: 'edit/:id',
    component: AgencyCreateComponent,
  },
  {
    path: 'view/:viewId',
    component: AgencyCreateComponent,
  },
];

@NgModule({
  declarations: [
    AgencyCreateComponent,
    AgencyListComponent,

  
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    RouterModule.forChild(routes),
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    TranslateModule
  ],
  exports: [
    AgencyCreateComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AgencyModule { }
