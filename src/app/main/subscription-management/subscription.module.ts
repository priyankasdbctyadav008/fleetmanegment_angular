
import { CommonModule } from '@angular/common';
import { SubscriptionListComponent } from './subscription-list/subscription-list.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularMaterialModule } from 'src/app/material.module';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';


const routes:Routes=[
  {
    path:'list',
    component:SubscriptionListComponent,
  }
];



@NgModule({
  declarations: [
    SubscriptionListComponent
  ],
  imports: [
 
    CommonModule,
    AngularMaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    RouterModule.forChild(routes),

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SubscriptionModule { }
