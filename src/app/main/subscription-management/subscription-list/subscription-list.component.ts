import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatTable, MatTableModule } from '@angular/material/table';
import { AuthService } from 'src/app/services/api/api.service';
@Component({
  selector: 'app-subscription-list',
  templateUrl: './subscription-list.component.html',
  styleUrls: ['./subscription-list.component.css']
})
export class SubscriptionListComponent {
  vehicleBrand: any;
  @ViewChild('paginator') paginator!: MatPaginator;

  subscriptions: any;

  columnsToDisplay = ['name', 'stripePlanId', 'amount']
  dataSource!: MatTableDataSource<any>;

  constructor(private authService: AuthService) {
  }
  ngOnInit(): void {
    // this.getList();
  }

  getList() {
    try {
      this.authService.getSubscriptionList().then((res: any) => {
        console.log('res: ', res);
        this.subscriptions = res.data;
        this.dataSource = new MatTableDataSource(this.subscriptions);
        this.dataSource.paginator = this.paginator;

      })
    } catch (error) {
      console.log('error: ', error);

    }
  }

}
