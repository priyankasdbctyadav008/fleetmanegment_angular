import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationSettingComponent } from './reservation-setting.component';

describe('ReservationSettingComponent', () => {
  let component: ReservationSettingComponent;
  let fixture: ComponentFixture<ReservationSettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservationSettingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReservationSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
