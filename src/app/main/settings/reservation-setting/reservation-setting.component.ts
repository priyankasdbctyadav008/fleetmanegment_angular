import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/api/api.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';

@Component({
  selector: 'app-reservation-setting',
  templateUrl: './reservation-setting.component.html',
  styleUrls: ['./reservation-setting.component.css']
})
export class ReservationSettingComponent {
  reservationSettingForm: FormGroup;
  hoursArray: number[] = Array.from({ length: 24 }, (value, index) => index + 1);
  monthsArray: number[] = Array.from({ length: 12 }, (value, index) => index + 1);
  daysArray: number[] = Array.from({ length: 30 }, (value, index) => index + 1);
  durationInSeconds = 1;
  isSubmitted: boolean = false;
  id: any;
  reserveationId: any;


  constructor(private fb: FormBuilder, private authService: AuthService, private _snackBar: MatSnackBar) {
    this.reservationSettingForm = this.fb.group({
      informAboutRentalBeforeDays: ['', Validators.required],
      rentalReserveBeforeMonths: ['', Validators.required],
      minRentalPeriodInDays: ['', Validators.required],
      allowOverBooking: ['', Validators.required],
      authorizeOutsideReservationArea: ['', Validators.required],
      allowPastDateReservation: ['', Validators.required],
      isAutoGenerateBookingRef: ['', Validators.required],
      userId: ['']
    })
  }

  ngOnInit(): void {
    this.token();
  }

  token() {
    let data: any = localStorage.getItem('user');
    if (data) {
      data = JSON.parse(data);
      this.id = data.user.id;
      console.log(this.id)
      this.getResSettingById(this.id);

    }
  }

  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }

  getResSettingById(id: any) {
    try {
      this.authService.getSettingById(this.id).then((res: any) => {

        if (res.success === true) {
          let response = res.data[0];
          this.reserveationId = response.id;
          const category = this.reservationSettingForm.value;
          Object.keys(category).forEach((key) => {
            try {
              this.reservationSettingForm.controls[key].setValue(response[key]);

            } catch (error) {
              console.log('error: ', error);
            }
          });
        }
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }


  update() {
    try {
      this.authService.updateReserveSetting(this.reservationSettingForm.value, this.reserveationId).then((res: any) => {
        this.openSnackBar(res.message, 'true')
      }, (error) => {
        console.log('error: ', error);
      });
    } catch (error) {
      console.log('error: ', error);

    }
  }


  submit() {
    this.isSubmitted = true;
    if (!this.reservationSettingForm.valid) {
      const radioElements = document.getElementsByClassName('radio_group');
      for (let i = 0; i < radioElements.length; i++) {
        radioElements[i].classList.add('custom-error');

      }
    }
    if (this.reserveationId) {
      this.update();
    } else {
      if (this.reservationSettingForm.valid) {
        this.reservationSettingForm.value.userId = this.id;

        try {
          this.authService.reserveSettingCre(this.reservationSettingForm.value).then((res: any) => {
            this.openSnackBar(res.message, 'true');
          }, (error) => {
            console.log('error: ', error);
          });
        } catch (error) {
          console.log('error: ', error);
        }
      }
    }
  }
}
