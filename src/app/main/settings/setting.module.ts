
import { CommonModule } from '@angular/common';
import { ReservationSettingComponent } from './reservation-setting/reservation-setting.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularMaterialModule } from 'src/app/material.module';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';


export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

const routes:Routes=[
  {
    path:'reservation',
    component:ReservationSettingComponent,
  }
];


@NgModule({
  declarations: [
    ReservationSettingComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    TranslateModule,
    RouterModule.forChild(routes),

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SettingModule { }
