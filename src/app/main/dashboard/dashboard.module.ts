import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SuperDashboardComponent } from './super-dashboard/super-dashboard.component';
import { AngularMaterialModule } from 'src/app/material.module';
import { AuthGuard } from 'src/app/auth.guard';


const dashboardsRoutes: Routes = [
  {
    path: 'overview',
    component: SuperDashboardComponent
  }
];

@NgModule({
  imports: [
    AngularMaterialModule,
    RouterModule.forChild(dashboardsRoutes),
  ],
  declarations: [
    SuperDashboardComponent
  ],
  exports: [
    SuperDashboardComponent
  ],
  providers:[AuthGuard],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardsModule {
}
