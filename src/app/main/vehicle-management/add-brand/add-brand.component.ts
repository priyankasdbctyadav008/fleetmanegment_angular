import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { AuthService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-add-brand',
  templateUrl: './add-brand.component.html',
  styleUrls: ['./add-brand.component.css'],
})
export class AddBrandComponent {
  addBrandForm!: FormGroup;
  id: any;
  viewId: any;
  vehicleBrand: any;
  vehicleType: any;
  durationInSeconds = 1;
  vehicleCategory: any
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private location: Location,
    private router: Router
  ) {

    this.addBrandForm = this.fb.group({
      name: [''],
      vehiclesTypeId: [''],
      vehiclesCategoryId: [''],
      status: [false],
      description: ['vehicleeeeeeeeeee']
    });
  }

  ngOnInit() {
    this.categoryDropDown();
    this.id = this.route.snapshot.params['id'];
    this.viewId = this.route.snapshot.params['viewId'];
    if (this.id) {
      this.getBrandById(this.id);
    } else if (this.viewId) {
      this.getBrandById(this.id);
    }
  }

  back() {
    this.location.back();
  }

  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }


  categoryDropDown() {
    try {
      this.authService.getCategoryDropDown().then((res: any) => {
        this.vehicleCategory = res.data;
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }


  getBrandById(id: any) {
    try {
      this.authService.getBrandById(id).then((res: any) => {
        let response = res.data.vehiclesBrandMst;
        const BrandData = this.addBrandForm.value;
        this.onChangeCategory(response.vehiclesCategoryId)
        Object.keys(BrandData).forEach((key) => {
          try {
            if (key === 'status') {
              if (response.status === '1') {
                this.addBrandForm.controls['status'].setValue(1);
              } else {
                this.addBrandForm.controls['status'].setValue(0);
              }
            } else {
              this.addBrandForm.controls[key].setValue(response[key]);
            }
          } catch (error) {
            console.error('error: ' + key + '' + error);
          }
        });
      });
    } catch (error) {
      console.error('error: ', error);
    }
  }

  onChangeCategory(id: any) {
    console.error('id:5456656 ', id);
    try {
      this.getTypeDropDown(id);
    } catch (error) {
      console.error(error);
    }
  }


  getTypeDropDown(id: any) {
    try {
      this.authService.getTypeDropDown(id).then((res: any) => {
        this.vehicleType = res.data;
      });
    } catch (error) {
      console.error('error: ', error);
    }
  }

  submit() {
    if (this.id) {
      this.updateBrand();
    } else {
      if (this.addBrandForm.valid) {
        try {
          if (this.addBrandForm.value.status) {
            this.addBrandForm.value.status = '1';
          } else {
            this.addBrandForm.value.status = '0';
          }
          this.authService
            .createVehicleBrand(this.addBrandForm.value)
            .then((res: any) => {
              this.openSnackBar(res.message, 'true')
              localStorage.setItem('tabIndex', '2')
              this.router.navigate(['/vehicle/config']);
            });
        } catch (error: any) {
          this.openSnackBar(error.message, 'true')
        }
      }
    }
  }

  updateBrand() {
    try {
      if (this.addBrandForm.value.status) {
        this.addBrandForm.value.status = '1';
      } else {
        this.addBrandForm.value.status = '0';
      }
      this.authService.updateVehicleBrand(this.id, this.addBrandForm.value).then((res: any) => {

        this.openSnackBar(res.message, 'true')
        localStorage.setItem('tabIndex', '2')
        this.router.navigate(['/vehicle/config']);
      });
    } catch (error: any) {
      this.openSnackBar(error.message, 'true')
    }
  }
}