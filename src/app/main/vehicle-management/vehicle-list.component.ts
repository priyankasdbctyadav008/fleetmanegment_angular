import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { DialogBoxVehicleComponent } from 'src/app/helper/dialog-box-vehicle/dialog-box-vehicle.component';
import { AuthService } from 'src/app/services/api/api.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})

export class VehicleListComponent {

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  vehicles: any = [];
  searchVal: any = '';
  dataSource!: MatTableDataSource<any>;
  columnsToDisplay = ['id', 'model', 'registrationNo', 'vehicleCategoryId', 'agencyId', 'motorization', 'gearBox', 'actions'];
  durationInSeconds = 1;
  vehicle_data: any = [];
  Org: any;
  constructor(private authService: AuthService, public dialog: MatDialog, private _snackBar: MatSnackBar, private router: Router
  ) {
    let data: any = localStorage.getItem('user')
    let userData = JSON.parse(data);
    this.Org = userData.organizations_details[0];
  }

  ngOnInit() {
    this.getListById(this.searchVal, "", this.Org.id)
  }


  filterData(searchVal: any) {
    const input = searchVal.target.value;
    if (input.length >= 3) {
      this.getListById(this.searchVal, "model", this.Org.id)
    }
  }


  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }


  async getListById(searchVal: any, searchKey: any, id: any) {
    try {
      let data = { searchKey, searchVal, }
      this.authService.vehicleListbyId(data, id).then((res: any) => {
        this.vehicles = res.data;
        this.vehicle_data = res.total_count;
        this.dataSource = new MatTableDataSource(this.vehicles);
        this.dataSource.paginator = this.paginator;
        if (this.vehicles != undefined) {
          for (let i = 0; i <= this.vehicles.length; i++) {
            if (this.vehicles[i]) {
              this.vehicles[i].gearBox = JSON.parse(this.vehicles[i]?.gearBox);
              let dataGear = Object.keys(this.vehicles[i].gearBox);
              this.vehicles[i].gearBox = dataGear;
              this.vehicles[i].motorization = JSON.parse(this.vehicles[i]?.motorization);
              // let dataMotor = Object.keys(this.vehicles[i].motorization);
              // this.vehicles[i].motorization = dataMotor;
            }
          }
        }
      })
    } catch (error) {
      console.log(error);
    }

  }


  deleteVehicle(id: any) {
    try {
      this.authService.deleteVehicle(id).then((res: any) => {
        if (res.success == true) {
          this.openSnackBar(res.message, 'true')
        }
        this.getListById(this.searchVal, "model", this.Org.id);
      })
    } catch (error) {
      console.log('error: ', error);

    }
  }


  openDialog(dialogData: any): void {
    const dialogRef = this.dialog.open(DialogBoxVehicleComponent, {
      data: dialogData,
      height: '607px',
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log('The dialog was closed');
    });
  }


}
