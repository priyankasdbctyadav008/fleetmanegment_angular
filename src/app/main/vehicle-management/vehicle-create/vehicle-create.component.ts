import { ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogBoxVehicleComponent } from 'src/app/helper/dialog-box-vehicle/dialog-box-vehicle.component';
import { AuthService } from 'src/app/services/api/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe, Location } from '@angular/common';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { MaxDaysValidator } from 'validator';

@Component({
  selector: 'app-vehicle-create',
  templateUrl: './vehicle-create.component.html',
  styleUrls: ['./vehicle-create.component.css'],
  providers: [TranslatePipe, DatePipe]
})
export class VehicleCreateComponent {
  @ViewChild('fileInput', { static: false }) myFileInput!: ElementRef;
  selectedImage: any;
  fileObject: any;
  images: any = [];
  myForm: FormGroup;
  fileUploadId: any;
  isChecked = false;
  currency: any;
  disabled = false;
  toggleValue = false;
  checkboxValid: boolean = false;
  vehicleCategory: any;
  vehicleType: any;
  vehicleBrand: any;
  deleteMultipleImageIndex: any = [];
  updatedGear: any;
  updateMotor: any;
  previewsImage: any = [];
  Agencies: any;
  image_url1: any;
  image_url2: any;
  image_url3: any;
  image_url4: any;
  image_url5: any;
  image_url6: any;
  id: any;
  viewId: any;
  imageSrc1: any;
  imageSrc2: any;
  imageSrc3: any;
  imageSrc4: any;
  imageSrc5: any;
  multiImage: any = [];
  multiImageImg: any = [];
  date = new Date();
  currentYear = this.date.getFullYear();
  maxDate: any = Date;
  yearsArray: number[] = Array.from({ length: this.currentYear - 1900 + 1 }, (value, index) => this.currentYear - index);
  motorization: { [key: string]: string } = {
    diesel: 'diesel',
    petrol: 'petrol',
    electric: 'electric',
    hybrid: 'hybrid',
    SP_gasonline: 'SP_gasonline',
    other: 'other',
  };
  gearBox: { [key: string]: string } = {
    manual: 'manual',
    automatic: 'automatic',
  };

  durationInSeconds = 1;
  Org: any;
  vehicleFeatures = {
    Gps: 'no',
    Reversing_Radar: 'no',
    Bluetooth: 'no',
    Air_Conditioner: 'no',
    isofix_Seat: 'no',
  };
  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private router: Router,
    private authService: AuthService, private cdr: ChangeDetectorRef, private route: ActivatedRoute,
    private location: Location, public translate: TranslateService, public _translatePipe: TranslatePipe,
    private datePipe: DatePipe
  ) {
    let data: any = localStorage.getItem('user')
    let userData = JSON.parse(data);
    this.Org = userData.organizations_details[0];

    this.myForm = this.fb.group({
      vehicleCategoryId: ['', Validators.required],
      vehicleTypeId: ['', Validators.required],
      vehicleBrandId: [''],
      model: [''],
      noOfDoors: [],
      vehicleFeatures: [this.vehicleFeatures, [this.atLeastOneCheckedValidator.bind(this)]],
      agencyId: [''],
      registrationNo: [''],
      yearOfRegistration: [''],
      motorization: [''],
      gearBox: [''],
      noOfSeats: [],
      mileage: [''],
      lastInspectionDate: [''],
      lastRevisionDate: [''],
      ratePerDay: [''],
      miniMumDay: [],
      maxiMumDay: [],
      status: [''],
      image_url1: [null, Validators.required],
      image_url2: [null, Validators.required],
      image_url3: [null, Validators.required],
      image_url4: [null, Validators.required],
      image_url5: [null, Validators.required],
      image_url6: [null, [Validators.required]],
    },
      {
        validators: MaxDaysValidator('miniMumDay', 'maxiMumDay'),
      }
    );
  }


  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.categoryDropDown();
    this.brandDropDown();
    this.getList();
    if (this.id) {
      this.getVehicle(this.id);
    }else if(this.viewId){
      this.getVehicle(this.viewId);
    }
    this.maxDate = new Date(this.currentYear, this.date.getMonth(), this.date.getDate());
    let prefferedCurrencies = JSON.parse(this.Org.prefferedCurrencies);
    this.currency = prefferedCurrencies.symbol
  }

  back() {
    this.location.back();
  }


  atLeastOneCheckedValidator() {
    const agency: any[] = this.myForm?.controls['vehicleFeatures'].value;
    if (agency) {

      const isChecked = Object.values(agency).some((value) => value === 'yes');
      if (isChecked) {
        return false;
      } else {
        return { atLeastOneCheckedError: true };
      }
    } else {
      return { atLeastOneCheckedError: true };
    }
  }


  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }


  updateFeature(feature: string, event: any) {
    const vehicleFeatures = this.myForm.get('vehicleFeatures');
    if (vehicleFeatures) {
      this.myForm.value.vehicleFeatures[feature] = event.checked ? 'yes' : 'no';
      vehicleFeatures.patchValue(this.myForm.value.vehicleFeatures);
      this.updateCheckboxValidity();
    }
  }


  updateCheckboxValidity() {
    const vehicleFeatures: any = this.myForm?.controls['vehicleFeatures'].value;
    if (vehicleFeatures) {
      const isChecked = Object.values(vehicleFeatures).some((value) => value === 'yes');
      this.checkboxValid = !isChecked;
    }
  }


  categoryDropDown() {
    try {
      this.authService.getCategoryDropDown().then((res: any) => {
        this.vehicleCategory = res.data;
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  onChangeCategory(value: any) {
    console.log('value: ', value);
    try {
      this.typeDropDown(value.value);
    } catch (error) {
      console.error(error);
    }
  }

  typeDropDown(id: any) {
    try {
      this.authService.getTypeDropDown(id).then((res: any) => {
        this.vehicleType = res.data;
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }


  brandDropDown() {
    try {
      this.authService.vehicleBrandDropdown().then((res: any) => {
        this.vehicleBrand = res.data;
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }



  getGearBoxKeys(): string[] {
    return Object.keys(this.gearBox);
  }


  getMotorizationKey(): string[] {
    return Object.keys(this.motorization);
  }


  updateSelectedGearBox(selectedValue: any) {
    const selectedKey = Object.keys(this.gearBox).find(
      (key) => this.gearBox[key] === selectedValue
    );
    if (selectedKey) {
      const updatedGearBox = {
        [selectedKey]: this.gearBox[selectedKey],
      };
      this.updatedGear = updatedGearBox;
    }
  }


  updateMotorization(selectedValue: any) {
    const selectedKey = Object.keys(this.motorization).find(
      (key) => this.motorization[key] === selectedValue
    );
    if (selectedKey) {
      const updatedMotorization = {
        [selectedKey]: this.motorization[selectedKey],
      };

      this.updateMotor = updatedMotorization;
    }
  }

  getList() {
    try {
      this.authService.agencyDropDown(this.Org.id).then((res: any) => {
        this.Agencies = res.data;
      })
    } catch (error) {
      console.log(error);
    }
  }


  uploadImage(event: any, index: any): void {
    try {
      if (event.target.files && event.target.files[0]) {
        const file = event.target.files[0];
        const allowedTypes = ['image/jpeg', 'image/png', 'image/svg', 'application/pdf'];
        if (allowedTypes.includes(file.type)) {
          if (file.size <= 1024 * 1024) {
            console.log('file.size <= 1024 * 1024: ', file.size <= 1024 * 1024);
            let path = 'http://192.168.0.60/fleet-management/api/public/uploads/'
            this.authService.uploadImage(file, '').then((res: any) => {
              if (index == '1') {
                this.image_url1 = res.data.data[0].name;
                this.myForm.controls['image_url1'].setValue(res.data.data[0].name);
                this.imageSrc1 = `${path}${res.data.data[0].name}`;
              } else if (index == '2') {
                this.image_url2 = res.data.data[0].name;
                this.myForm.controls['image_url2'].setValue(res.data.data[0].name);
                this.imageSrc2 = `${path}${res.data.data[0].name}`;
              } else if (index == '3') {
                this.image_url3 = res.data.data[0].name;
                this.myForm.controls['image_url3'].setValue(res.data.data[0].name);
                this.imageSrc3 = `${path}${res.data.data[0].name}`;
              } else if (index == '4') {
                this.image_url4 = res.data.data[0].name;
                this.myForm.controls['image_url4'].setValue(res.data.data[0].name);
                this.imageSrc4 = `${path}${res.data.data[0].name}`;
              } else if (index == '5') {
                this.image_url5 = res.data.data[0].name;
                this.myForm.controls['image_url5'].setValue(res.data.data[0].name);
                this.imageSrc5 = `${path}${res.data.data[0].name}`;
              }
            }, (error) => {
              this.openSnackBar(error.response.data.message, 'false');
            })
          } else {
            this.openSnackBar('File size is greater than 1mb', 'false');
          }
        } else {
          this.openSnackBar('File is only support image or pdf formate', 'false');
        }
      }
    } catch (error) {
      console.error('error: ', error);
    }
  }


  onFileChange(event: any) {
    const files = event.target.files;
    const allowedTypes = ['image/jpeg', 'image/png', 'image/svg', 'application/pdf'];
    if (allowedTypes.includes(files[0].type)) {
      if (files[0].size <= 1024 * 1024) {
        const maxFilesAllowed = 4;
        const totalImages = this.images.length + files.length;
        if (totalImages <= maxFilesAllowed) {
          let file: any
          for (let i = 0; i < files.length; i++) {
            file = files[i];
            this.previewsImage.push(file);
            this.images.push(file)
          }
          this.upload(this.previewsImage);
          this.previewsImage = [];

        } else {
          this.openSnackBar('Only 4 Documents Can Be Uploaded', 'false');
        }
      } else {
        this.openSnackBar('File size is greater than 1mb', 'false');
      }
    } else {
      this.openSnackBar('File is only support image or pdf formate', 'false');
    }
  }


  upload(file: File): void {
    try {
      this.authService.uploadImage(file, 'multi').then((res: any) => {
        if (res.data.success === true) {
          res.data.data.map((item: any) => {
            this.multiImageImg.push(item.name);
          });
          this.multiImageImg = this.multiImageImg.filter((value: any) => {
            return typeof value === 'string'
          })
          this.image_url6 = this.multiImageImg;
          this.displaySrcMulti(this.multiImageImg)
          this.myForm.controls['image_url6'].setValue(JSON.stringify(this.multiImageImg));
        } else {
          this.openSnackBar(res.data.message, 'false');
        }
      }, (error) => {
        this.openSnackBar(error.response.data.message, 'false');
      });

    } catch (error) {
      console.error('error: ', error);
    }
  }


  deleteImage(index: any): void {
    if (index == '1') {
      this.displaySrc('', index);
      this.deleteMultipleImageIndex.push(this.image_url1)
      // this.authService.removeImage(this.image_url1).then((res: any) => {
      this.myForm.controls['image_url1'].setValue('');
      this.image_url1 = '';
      this.openSnackBar('Image Deleted', 'true');
      // })
      console.log('this.image_url1: ', this.image_url1);
    } else if (index == '2') {
      this.displaySrc('', index);
      // this.authService.removeImage(this.image_url2).then((res: any) => {
      this.deleteMultipleImageIndex.push(this.image_url2)
      this.myForm.controls['image_url2'].setValue('');
      this.image_url2 = '';
      this.openSnackBar('Image Deleted', 'true');

      // })
    } else if (index == '3') {
      this.displaySrc('', index);
      // this.authService.removeImage(this.image_url3).then((res: any) => {
      this.deleteMultipleImageIndex.push(this.image_url3)
      this.myForm.controls['image_url3'].setValue('');
      this.image_url3 = '';
      this.openSnackBar('Image Deleted', 'true');

      // })
    } else if (index == '4') {
      this.displaySrc('', index);
      // this.authService.removeImage(this.image_url4).then((res: any) => {
      this.deleteMultipleImageIndex.push(this.image_url4)
      this.myForm.controls['image_url4'].setValue('');
      this.image_url4 = '';
      this.openSnackBar('Image Deleted', 'true');
      // })
    } else if (index == '5') {
      this.displaySrc('', index);
      // this.authService.removeImage(this.image_url5).then((res: any) => {
      this.deleteMultipleImageIndex.push(this.image_url5)
      this.myForm.controls['image_url5'].setValue('');
      this.image_url5 = '';
      this.openSnackBar('Image Deleted', 'true');
      // })
    }
  }


  deleteMultiImage(index: any) {
    let formData = this.myForm.controls['image_url6'].value;
    let datavalue = JSON.parse(formData);
    this.deleteMultipleImageIndex.push(datavalue[index])
    this.images = []
    this.multiImageImg = []
    datavalue.splice(index, 1);
    for (let i = 0; i < datavalue.length; i++) {
      this.images.push(datavalue[i]);
      this.multiImageImg.push(datavalue[i]);
    }
    this.myForm.controls['image_url6'].setValue(JSON.stringify(this.multiImageImg));
    this.displaySrcMulti(this.multiImageImg);
    this.openSnackBar('Image Deleted', 'true');
  }


  submit() {
    this.myForm.markAllAsTouched();
    let data: any = this.atLeastOneCheckedValidator();
    this.checkboxValid = data.atLeastOneCheckedError;
    console.log('this.myForm.: ', this.myForm);

    if (this.id) {
      this.updateVehicle();
    }
    else {
      try {
        if (this.myForm.valid) {
          let data = JSON.parse(JSON.stringify(this.myForm.value));
          if (data.status) {
            data.status = '1';
          } else {
            data.status = '0';
          }
          data.ratePerDay = `${this.currency + ' ' + this.myForm.value.ratePerDay}`;
          data.vehicleFeatures = JSON.stringify(data.vehicleFeatures);
          data.gearBox = JSON.stringify(this.updatedGear);
          data.motorization = JSON.stringify(this.updateMotor);
          this.authService
            .createVehicleAgency(data)
            .then((res: any) => {
              if (res.success == true) {
                this.openDialog(res.data)
                this.router.navigate(['/vehicle/list']);
              }
            });
        }
      } catch (error) {
        console.log('error: ', error);
      }
    }
  }


  displaySrc(src: any, index: any) {
    let srcUrl
    if (src === '') {
      srcUrl = src
    } else {
      srcUrl = `http://192.168.0.60/fleet-management/api/public/uploads/${src}`;
    }

    if (index == '1') {
      this.imageSrc1 = srcUrl;
    } else if (index == '2') {
      this.imageSrc2 = srcUrl;
    } else if (index == '3') {
      this.imageSrc3 = srcUrl;
    } else if (index == '4') {
      this.imageSrc4 = srcUrl;
    } else if (index == '5') {
      this.imageSrc5 = srcUrl;
    }
  }


  displaySrcMulti(data: any) {
    this.multiImage = [];
    data.map((value: any) => {
      this.multiImage.push(`http://192.168.0.60/fleet-management/api/public/uploads/${value}`)
    })
  }


  getVehicle(id: any) {
    try {
      this.authService.getVehicleById(id).then((res: any) => {
        let response = res.data.agencyVehicles;
        const editFormValue = this.myForm.value;
        Object.keys(editFormValue).forEach((key) => {
          try {
            if (key === 'vehicleFeatures') {
              this.myForm.controls[key].setValue(JSON.parse(response.vehicleFeatures));

            } else if (key === 'motorization') {
              const motor = JSON.parse(response.motorization);
              this.updateMotor = motor;
              let motorization = Object.keys(motor);
              this.myForm.controls[key].setValue(motorization[0]);
            } else if (key === 'gearBox') {
              const gear = JSON.parse(response.gearBox);
              this.updatedGear = gear;
              let gearBox = Object.keys(gear);
              this.myForm.controls[key].setValue(gearBox[0]);
            }
            else if (key === 'image_url1') {
              this.image_url1 = response.image_url1;
              this.myForm.controls[key].setValue(this.image_url1);
              this.displaySrc(response.image_url1, '1');
            }
            else if (key === 'image_url2') {
              this.image_url2 = response.image_url2;
              this.myForm.controls[key].setValue(this.image_url2);
              this.displaySrc(response.image_url2, '2');
            }
            else if (key === 'image_url3') {
              this.image_url3 = response.image_url3;
              this.myForm.controls[key].setValue(this.image_url3);
              this.displaySrc(response.image_url3, '3');
            }
            else if (key === 'image_url4') {
              this.image_url4 = response.image_url4;
              this.myForm.controls[key].setValue(this.image_url4);

              this.displaySrc(response.image_url4, '4');
            }
            else if (key === 'image_url5') {
              this.image_url5 = response.image_url5;
              this.myForm.controls[key].setValue(this.image_url5);
              this.displaySrc(response.image_url5, '5');
            }
            else if (key === 'image_url6') {
              let data = JSON.parse(response.image_url6);
              this.image_url6 = data;
              this.images = data;
              this.multiImageImg = data;
              this.myForm.controls[key].setValue(JSON.stringify(this.image_url6));
              this.displaySrcMulti(data)
            }
            else {
              this.myForm.controls[key].setValue(response[key]);
            }
          } catch (error) {
            console.error('error: ', error);
          }
        });
      }, (error) => {
        console.error('error: ', error);
      });
    } catch (error) {
      console.error('error: ', error);
    }
  }


  updateVehicle() {
    console.log('this.deleteMultipleImageIndex', this.deleteMultipleImageIndex);
    this.deleteMultipleImageIndex.map((img: any) => {
      this.authService.removeImage(img).then((res: any) => {

      })
    })
    this.deleteMultipleImageIndex = [];
    if (this.myForm.valid) {
      try {

        if (this.myForm.value.status) {
          this.myForm.value.status = '1';
        } else {
          this.myForm.value.status = '0';
        }
        if (this.multiImageImg.length > 0) {
          this.myForm.controls['image_url6'].setValue(JSON.stringify(this.multiImageImg));
        } else {
          this.myForm.controls['image_url6'].setValue('');
        }
        this.myForm.value.vehicleFeatures = JSON.stringify(this.myForm.value.vehicleFeatures);
        this.myForm.value.gearBox = JSON.stringify(this.updatedGear);
        this.myForm.value.motorization = JSON.stringify(this.updateMotor);

        this.authService.vehicleUpdate(this.myForm.value, this.id).then((res: any) => {
          if (res.success == true) {
            this.openSnackBar('Vehicle Updated', 'true');
            this.router.navigate(['/vehicle/list']);
          }
        }, (error) => {
          console.error('error: ', error);
          this.openSnackBar('Server Error', 'false');
        })
      } catch (error) {
        this.openSnackBar('Server Error', 'false');
      }
    }
  }


  openDialog(dialogData: any): void {
    const dialogRef = this.dialog.open(DialogBoxVehicleComponent, {
      data: dialogData,
      height: '607px',
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log('The dialog was closed');
    });
  }
}
