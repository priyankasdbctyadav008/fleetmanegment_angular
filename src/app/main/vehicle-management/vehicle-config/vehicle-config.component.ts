import { ViewEncapsulation } from '@angular/compiler';
import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabGroup } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { AuthService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-vehicle-config',
  templateUrl: './vehicle-config.component.html',
  styleUrls: ['./vehicle-config.component.css']
})
export class VehicleConfigComponent implements OnInit {
  @Output() clicked = new EventEmitter<void>();
  @ViewChild('tabGroup') tabGroup!: MatTabGroup;


  selectedTab: string = 'category'; // Initialize with the default tab value
  currentComponent = 1;
  vehicleType: any = [];
  vehicleCategory: any = [];
  vehicleBrand: any = [];
  durationInSeconds: number = 1;
  selectedTabIndex: any = 0;
  searchVal: any = '';
  showComponent(componentNumber: number) {
    this.currentComponent = componentNumber;

    console.log(this.clicked.emit());
  }

  // dataSource!:MatTableDataSource<any>;
  @ViewChild('paginator') paginator!: MatPaginator;

  dataSourceType: any;
  dataSourceCategory: any;
  dataSourceBrand: any;
  columnsToDisplayBrand: any;
  columnsToDisplayCategory: any;
  columnsToDisplayType: any;
  constructor(private cdr: ChangeDetectorRef, private authService: AuthService,
    private _snackBar: MatSnackBar, private route: ActivatedRoute, private router: Router) { }

  ngAfterContentChecked(): void {
    this.cdr.detectChanges();
  }


  ngOnInit(): void {
    const tabIndex = localStorage.getItem('tabIndex');
    this.selectedTabIndex = tabIndex;
    this.getVehicleCategoryList(this.searchVal, "");
    this.getVehicleTypeList(this.searchVal, "");
    this.getVehicleBrandList(this.searchVal, "");
    this.columnsToDisplayCategory = ['vehicle_category','isActive', 'actions'];
    this.columnsToDisplayType = ['vehicle_type','Category', 'isActive', 'actions'];
    this.columnsToDisplayBrand = ['vehicle_brand', 'Category','Type','isActive', 'actions'];
  }


  onClick() {
    this.clicked.emit();
  }

  onTabChange(event: any) {
    localStorage.setItem('tabIndex', event.index);
    this.selectedTabIndex = event.index;
    console.log('this.selectedTabIndex: ', this.selectedTabIndex);
  }

  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }


  getVehicleTypeList(searchVal: any, searchKey: any) {
    try {
      let data = { searchKey, searchVal }
      this.authService.vehicleTypeList(data).then((res: any) => {
        this.vehicleType = res.data;
        this.dataSourceType = new MatTableDataSource(this.vehicleType);
        this.dataSourceType.paginator = this.paginator;
      })
    } catch (error) {
      console.log(error);
    }
  }


  getVehicleCategoryList(searchVal: any, searchKey: any) {
    try {
      let data = { searchKey, searchVal }
      this.authService.vehicleCategoryList(data).then((res: any) => {
        this.vehicleCategory = res.data;
        this.dataSourceCategory = new MatTableDataSource(this.vehicleCategory);
        this.dataSourceCategory.paginator = this.paginator;
      })
    } catch (error) {
      console.log('error: ', error);
    }
  }


  getVehicleBrandList(searchVal: any, searchKey: any) {
    try {
      let data = { searchKey, searchVal }
      this.authService.vehicleBrandList(data).then((res: any) => {
        this.vehicleBrand = res.data;
        this.dataSourceBrand = new MatTableDataSource(this.vehicleBrand);
        this.dataSourceBrand.paginator = this.paginator;
      })
    } catch (error) {
      console.log('error: ', error);
    }
  }


  deleteCategory(id: any) {
    try {
      this.authService.deleteVehicleCategory(id).then((res: any) => {
        this.getVehicleCategoryList(this.searchVal, "name");
        if (res.success === true) {
          this.openSnackBar(res.message, 'true')
        }
        else {
          this.openSnackBar(res.message, 'false')
        }
      })
    } catch (error) {
      console.log('error: ', error);

    }
  }

  deleteType(id: any) {
    try {
      this.authService.deleteVehicleType(id).then((res: any) => {
        this.getVehicleTypeList(this.searchVal, "name");
        if (res.success === true) {
          this.openSnackBar(res.message, 'true')
        }
        else {
          this.openSnackBar(res.message, 'false')
        }
      })
    } catch (error) {
      console.log('error: ', error);
    }
  }

  deleteBrand(id: any) {
    try {
      this.authService.deleteVehicleBrand(id).then((res: any) => {
        this.getVehicleBrandList(this.searchVal, "name");
        if (res.success === true) {
          this.openSnackBar(res.message, 'true')
        }
        else {
          this.openSnackBar(res.message, 'false')
        }
      })
    } catch (error) {
      console.log('error: ', error);
    }
  }

  filterData(searchVal: any, tab: any) {
    if (tab === 'category') {
      this.getVehicleCategoryList(this.searchVal, "name")
    } else if (tab === 'type') {
      this.getVehicleTypeList(this.searchVal, "name")
    } else if (tab === 'brand') {
      this.getVehicleBrandList(this.searchVal, "name")
    }
  }
}