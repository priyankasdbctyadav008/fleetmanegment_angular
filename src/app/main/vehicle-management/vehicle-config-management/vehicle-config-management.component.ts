import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: '[app-vehicle-config-management]',
  templateUrl: './vehicle-config-management.component.html',
  styleUrls: ['./vehicle-config-management.component.css']
})
export class VehicleConfigManagementComponent {
  @Input() showComponent: any;
  constructor(private route: ActivatedRoute){
  }

  item: any;
  
  ngOnInit() {
    this.item = this.route.snapshot.paramMap.get('type');
    console.log('this.item999999999', this.item);
  }
}
