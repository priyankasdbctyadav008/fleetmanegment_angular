import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleConfigManagementComponent } from './vehicle-config-management.component';

describe('VehicleConfigManagementComponent', () => {
  let component: VehicleConfigManagementComponent;
  let fixture: ComponentFixture<VehicleConfigManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleConfigManagementComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VehicleConfigManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
