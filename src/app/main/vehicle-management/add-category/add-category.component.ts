import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { AuthService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent {
  addCategoryForm!: FormGroup
  vehicleCategory: any;
  id: any;
  viewId:any
  durationInSeconds=1;

  constructor(private fb: FormBuilder, private authService: AuthService, private location: Location,
     private route: ActivatedRoute, private router: Router,private _snackBar: MatSnackBar) {
    this.addCategoryForm = this.fb.group({
      name: [''],
      status: [false]
    })
  }

  ngOnInit() {
    this.categoryDropDown();
    this.id = this.route.snapshot.params['id'];
    this.viewId = this.route.snapshot.params['viewId'];
    if (this.id) {
      this.getCategoryById(this.id);
    }else if(this.viewId){
      this.getCategoryById(this.viewId);
    }
  }


  openSnackBar(value: any,type:any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess:type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }

  back() {
    this.location.back();
  }


  submit() {
    if (this.id) {
      this.updateCategory();
    } else {
      if(this.addCategoryForm.valid){
      try {
        if (this.addCategoryForm.value.status) {
          this.addCategoryForm.value.status = "1"
        } else {
          this.addCategoryForm.value.status = "0"
        }
        this.authService.createVehicleCategory(this.addCategoryForm.value).then((res: any) => {
          this.openSnackBar(res.message,'true');
          localStorage.setItem('tabIndex','0')
          this.router.navigate(['/vehicle/config']);
        })
      } catch (error) {
        console.log('error: ', error);
      }
    }
  }
  }


  categoryDropDown() {
    try {
      this.authService.getCategoryDropDown().then((res: any) => {
        this.vehicleCategory = res.data;
      })
    } catch (error) {
      console.log('error: ', error);
    }
  }


  getCategoryById(id: any) {
    try {
      this.authService.getCategoryById(id).then((res: any) => {
        let response = res.data.vehiclesCategoryMst;
        const category = this.addCategoryForm.value;
        Object.keys(category).forEach(key => {
          try {
            if (key === 'status') {
              if (response.status === '1') {
                this.addCategoryForm.controls['status'].setValue(1);
              } else {
                this.addCategoryForm.controls['status'].setValue(0);
              }
            } else {
              this.addCategoryForm.controls[key].setValue(response[key]);
            }
          } catch (error) {
            console.log('error: ', error);
          }
        });
      })
    } catch (error) {
      console.log('error: ', error);
    }
  }


  updateCategory() {
    if(this.addCategoryForm.valid){
    try {
      console.log(this.addCategoryForm.value)
      if (this.addCategoryForm.value.status) {
        this.addCategoryForm.value.status = "1"
      } else {
        this.addCategoryForm.value.status = "0"
      }
      this.authService.updateVehicleCategory(this.id, this.addCategoryForm.value).then((res: any) => {
        this.openSnackBar(res.message,'true');
        localStorage.setItem('tabIndex','0')
        this.router.navigate(['/vehicle/config']);
      })
    } catch (error) {
      console.log('error: ', error);
    }
  }
  }
}