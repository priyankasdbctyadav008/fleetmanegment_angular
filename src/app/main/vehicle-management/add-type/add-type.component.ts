import { Location } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { AuthService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-add-type',
  templateUrl: './add-type.component.html',
  styleUrls: ['./add-type.component.css'],
})
export class AddTypeComponent {
  addTypeForm: FormGroup;
  vehicleCategory: any;
  id: any;
  viewId: any;
  durationInSeconds = 1;

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {
    this.addTypeForm = this.fb.group({
      name: [''],
      vehiclesCategoryId: [''],
      status: [false],
    });
  }

  ngOnInit() {
    this.categoryDropDown();
    this.id = this.route.snapshot.params['id'];
    this.viewId = this.route.snapshot.params['viewId'];
    if (this.id) {
      this.getTypeById(this.id);
    } else if (this.viewId) {
      this.getTypeById(this.id);
    }
  }


  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }

  back() {
    this.location.back();
  }

  categoryDropDown() {
    try {
      this.authService.getCategoryDropDown().then((res: any) => {
        this.vehicleCategory = res.data;
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  getTypeById(id: any) {
    try {
      this.authService.getVehicleTypeById(id).then((res: any) => {
        let response = res.data.vehiclesTypeMst;
        const category = this.addTypeForm.value;
        Object.keys(category).forEach((key) => {
          try {
            if (key === 'status') {
              if (response.status === '1') {
                this.addTypeForm.controls['status'].setValue(1);
              } else {
                this.addTypeForm.controls['status'].setValue(0);
              }
            } else {
              this.addTypeForm.controls[key].setValue(response[key]);
            }
          } catch (error) {
            console.log('error: ', error);
          }
        });
      });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  submit() {
    if (this.id) {
      this.updateType();
    } else {
      if (this.addTypeForm.valid) {
        if (this.addTypeForm.value.status) {
          this.addTypeForm.value.status = '1';
        } else {
          this.addTypeForm.value.status = '0';
        }
        try {
          this.authService
            .createVehicleType(this.addTypeForm.value)
            .then((res: any) => {
              if (res.success === true) {
                console.log('res.message: ', res.message);
                this.openSnackBar(res.message, 'true');
                localStorage.setItem('tabIndex', '1')
                this.router.navigate(['/vehicle/config']);
              } else {
                console.log('res.message', typeof res.message);
                if (typeof res.message === 'object') {
                  let arr = Object.values(res.message);
                  if (Array.isArray(arr)) {
                    this.openSnackBar(arr[0], 'false');
                  } else {
                    this.openSnackBar(res.message, 'false');
                  }
                } else {
                  this.openSnackBar(res.message, 'true');
                }
              }
            });
        } catch (error) {
          console.log('error: ', error);
        }
      }
    }
  }

  updateType() {
    try {
      console.log(this.addTypeForm.value);

      if (this.addTypeForm.valid) {
        if (this.addTypeForm.value.status) {
          this.addTypeForm.value.status = '1';
        } else {
          this.addTypeForm.value.status = '0';
        }
        this.authService
          .updateVehicleType(this.id, this.addTypeForm.value)
          .then((res: any) => {
            this.openSnackBar(res.message, 'true');
            localStorage.setItem('tabIndex', '1')
            this.router.navigate(['/vehicle/config']);
          });
      }
    } catch (error) {
      console.log('error: ', error);
    }
  }
}
