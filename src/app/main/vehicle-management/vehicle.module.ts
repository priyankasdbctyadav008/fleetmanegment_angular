import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VehicleListComponent } from './vehicle-list.component';
import { RouterModule, Routes } from '@angular/router';
import { AngularMaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VehicleCreateComponent } from './vehicle-create/vehicle-create.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgToggleModule } from 'ng-toggle-button';
import { VehicleConfigComponent } from './vehicle-config/vehicle-config.component';
import { AddTypeComponent } from './add-type/add-type.component';
import { AddBrandComponent } from './add-brand/add-brand.component';
import { VehicleConfigManagementComponent } from './vehicle-config-management/vehicle-config-management.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}
const routes: Routes = [
  {
    path: 'list',
    component: VehicleListComponent,
    title:"Vehicle List-Fleet Management"
  },
  {
    path: 'create',
    component: VehicleCreateComponent,
    title: "Vehicle Creation-Fleet Management"
  },
  {
    path:'config',
    component:VehicleConfigComponent,
    title:"Vehicle Config-Fleet Mangement"
  },
  {
    path:'config_management/:type',
    component:VehicleConfigManagementComponent,
    title:"Vehicle Config-Fleet Mangement"
  },
  {
    path: 'edit/:id',
    component: VehicleCreateComponent,
    title:"Vehicle Config-Fleet Mangement"
  },
  {
    path: 'view/:viewId',
    component: VehicleCreateComponent,
    title:"Vehicle Config-Fleet Mangement"
  },
  {
    path: 'edit/:type/:id',
    component:VehicleConfigManagementComponent,
    title:"Vehicle Config-Fleet Mangement"
  },
  {
    path: 'view/:type/:viewId',
    component: VehicleConfigManagementComponent,
    title:"Vehicle Config-Fleet Mangement"
  },
];

@NgModule({
  declarations: [VehicleListComponent, VehicleCreateComponent,VehicleConfigComponent, AddCategoryComponent, AddTypeComponent, AddBrandComponent, VehicleConfigManagementComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    FlexLayoutModule,
    TranslateModule,
    RouterModule.forChild(routes),
    NgToggleModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VehicleModule { }
