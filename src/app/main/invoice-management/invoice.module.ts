import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AngularMaterialModule } from 'src/app/material.module';
import { InvoiceCreateComponent } from './invoice-create/invoice-create.component';


const routes: Routes = [
  {
    path: 'list',
    component: InvoiceListComponent,
  },
  {
    path:'create',
    component:InvoiceCreateComponent
  }
];

@NgModule({
  declarations: [
    InvoiceListComponent,
    InvoiceCreateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    RouterModule.forChild(routes),
  ],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class InvoiceModule { }
