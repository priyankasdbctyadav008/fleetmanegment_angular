import { Component, } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-invoice-create',
  templateUrl: './invoice-create.component.html',
  styleUrls: ['./invoice-create.component.css']
})
export class InvoiceCreateComponent {
  
  invoiceForm!:FormGroup;
 
  constructor(private fb:FormBuilder){
       this.invoiceForm=this.fb.group({
        Invoice:[''],
        Billing_Agency:[''],
        Due_Date:[''],
        booking:['']
       })
  }


}