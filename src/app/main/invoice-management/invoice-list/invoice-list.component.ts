import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.css']
})
export class InvoiceListComponent {
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Total number of invoices generated', price: '60', cols: 3, rows: 1 },
          { title: 'Invoices awaiting payment', price: 'DT 15', cols: 3, rows: 1 },
          { title: 'Invoices paid', price: 'DT 10', cols: 3, rows: 1 },

        ];
      }

      return [
        { title: 'Total number of invoices generated', price: '60', cols: 1, rows: 1 },
        { title: 'Invoices awaiting payment', price: 'DT 15', cols: 1, rows: 1 },
        { title: 'Invoices paid', price: 'DT 10', cols: 1, rows: 1 },

      ];
    })
  );

  @ViewChild('paginator') paginator!: MatPaginator;

  dialogTemplate: TemplateRef<any> | undefined;

  DATA = [
    { BookingID: '#1231231', Customer: 'Test ', Billing_Agency: 'Ace Rentals Services', Date_Of_Invoice: '28/04/2023', Amount: 'DT200', DueDate: '04/04/2023', Vehicle: 'Sedan BMW-M6 SS1234', Status: 'Paid', actions: '' },
    { BookingID: '#1231231', Customer: 'Test ', Billing_Agency: 'Ace Rentals Services', Date_Of_Invoice: '28/04/2023', Amount: 'DT200', DueDate: '04/04/2023', Vehicle: 'Sedan BMW-M6 SS1234', Status: 'UnPaid', actions: '' },
    { BookingID: '#1231231', Customer: 'Test ', Billing_Agency: 'Ace Rentals Services', Date_Of_Invoice: '28/04/2023', Amount: 'DT200', DueDate: '04/04/2023', Vehicle: 'Sedan BMW-M6 SS1234', Status: 'Paid', actions: '' },

  ];




  dataSource!: MatTableDataSource<any>;
  dataSourceInvoice!: MatTableDataSource<any>;
  columnsToDisplay = ['BookingID', 'Vehicle', 'Customer', 'Billing_Agency', 'Date_Of_Invoice', 'Amount', 'DueDate', 'Status', 'actions'];

  columnsToDisplayInvoice = ['Description', 'Vehicle_Details', 'Payment_Mode', 'Quantity', 'Unit_Price_HT', 'Total']
  constructor(private breakpointObserver: BreakpointObserver, private dialog: MatDialog) {

  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator
  }



  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.DATA);

    this.dataSourceInvoice = new MatTableDataSource();
  }


  openDialog(templateRef: TemplateRef<any>): void {
    if (this.dialog != undefined) {
      const dialogRef = this.dialog.open(templateRef, { height: '252px', width: '405px' });
      dialogRef.afterClosed().subscribe(result => {
      });
    }
  }


  openDiIn(templateRef: TemplateRef<any>): void {
    if (this.dialog != undefined) {
      const dialogRef = this.dialog.open(templateRef,
        { height: ' 570px', width: '840px' });
      dialogRef.afterClosed().subscribe(result => {
      });
    }
  }







}


