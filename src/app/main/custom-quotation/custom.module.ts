import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomCreateComponent } from './custom-create.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/material.module';
import { CustomQuotesListComponent } from './custom-quotes-list/custom-quotes-list.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CreateCustomerComponent } from '../reservations-management/create-customer/create-customer.component';
import { ReservationsModule } from '../reservations-management/reservations.module';
const routes: Routes = [
  {
    path: 'quotes-create',
    component: CustomCreateComponent,
  },
  {
    path: 'quotes-list',
    component: CustomQuotesListComponent,
  }
];

@NgModule({
  declarations: [CustomCreateComponent, CustomQuotesListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,FlexLayoutModule,ReservationsModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    CustomCreateComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomModule { }
