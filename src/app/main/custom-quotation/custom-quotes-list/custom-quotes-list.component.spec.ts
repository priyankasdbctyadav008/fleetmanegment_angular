import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomQuotesListComponent } from './custom-quotes-list.component';

describe('CustomQuotesListComponent', () => {
  let component: CustomQuotesListComponent;
  let fixture: ComponentFixture<CustomQuotesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomQuotesListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomQuotesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
