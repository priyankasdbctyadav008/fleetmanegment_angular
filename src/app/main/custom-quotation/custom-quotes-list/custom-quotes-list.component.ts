import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table'
import { AuthService } from 'src/app/services/api/api.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-custom-quotes-list',
  templateUrl: './custom-quotes-list.component.html',
  styleUrls: ['./custom-quotes-list.component.css'],
  providers: [DatePipe]
})
export class CustomQuotesListComponent {
  @ViewChild('paginator') paginator!: MatPaginator;

  quotation: any;
  dataSource!: MatTableDataSource<any>;
  search: any
  columnsToDisplay = ['startDate', 'endDate', 'source', 'quotationExpiryDate', 'returnAgencyId', 'amount', 'vehicleCategoryId'];

  constructor(private authService: AuthService, private date: DatePipe) {

  }

  ngOnInit() {
    // this.getList(this.search, "");
    // this.getCustom();
  }


  filterData() {
    this.getList(this.search, "customerId");
  }


  getList(searchVal: any, searchKey: any) {
    try {
      let data = { searchKey, searchVal }
      this.authService.getQuotationList(data).then((res: any) => {
        this.quotation = res.data;
        this.dataSource = new MatTableDataSource(this.quotation);
        this.dataSource.paginator = this.paginator;
        for (let i = 0; i <= this.quotation.length; i++) {
          if (this.quotation[i]) {
            let data = this.quotation[i].startDate;
            const formattedDate = this.date.transform(data, 'yyyy-MM-dd ');
            this.quotation[i].startDate = formattedDate;
            let data1 = this.quotation[i].endDate;
            const formattedDate1 = this.date.transform(data1, 'yyyy-MM-dd ');
            this.quotation[i].endDate = formattedDate1;
            let data2 = this.quotation[i].quotationExpiryDate;
            const formattedDate2 = this.date.transform(data2, 'yyyy-MM-dd ');
            this.quotation[i].quotationExpiryDate = formattedDate2;
          }
        }
      });
    } catch (error) {
      console.error('error: ', error);
    }
  }


  getCustom() {
    try {
      this.authService.getCustomQuote().then((res: any) => {

      })


    } catch (error) {
      console.error('error: ', error);

    }
  }

}