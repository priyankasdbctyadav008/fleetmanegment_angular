import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/api/api.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-custom-create',
  templateUrl: './custom-create.component.html',
  styleUrls: ['./custom-create.component.css'],
  providers: [DatePipe]
})
export class CustomCreateComponent {
  isLinear = false;
  quotationFormGroup: FormGroup;
  customerFormGroup: any;
  companyFormGroup: any;
  vehicleCategory: any;
  showCustomerContent = true;
  showCompanyContent = true;
  startDate: any;
  search: any;
  searching: any;
  customers: any;
  company: any;
  Countries: any = [];
  State: any = [];
  constructor(private fb: FormBuilder, private authService: AuthService, private datePipe: DatePipe) {
    this.quotationFormGroup = this.fb.group({
      //-----------------------------quotation-----------------------------------------------//
      startDate: [''],
      endDate: [''],
      source: ['1', Validators.required],
      quotationExpiryDate: [''],
      vehicleCategoryId: ['', Validators.required],
      departureAgencyId: [1, Validators.required],
      returnAgencyId: ['1', Validators.required],
      amount: ['', Validators.required],
      customerId: ['1'],

      //-----------------------------company-------------------------------------------------//
      name: [''],
      company_email: [''],
      company_phone: [''],
      sirenno: [''],
      company_address1: [''],
      isProfessional: ['true'],
      company_city: [''],
      company_stateId: [''],
      company_countryId: [''],
      company_postalCode: [''],

      //---------------------------------customer----------------------------------------------//
      licenseNo: [''],
      dob: [''],
      licenseCreationDate: [''],
      licenseImgFrontUrl: ['test'],
      licenseImgBackUrl: ['test'],
      email: ['test@mailinator.com'],
      phone: ['7676767676'],
      customerCompanyId: [''],
      userId: ['1'],
      firstName: [''],
      lastName: [''],
      postalCode: [''],
      countryId: [''],
      stateId: [''],
      city: [''],
      address1: ['']

    });

  }


  ngOnInit() {
    this.categoryDropDown();
    this.getCustomerList('');
    this.getCompanyList('');
    this.countryList();

  }

  categoryDropDown() {
    try {
      this.authService.getCategoryDropDown().then((res: any) => {
        this.vehicleCategory = res.data;
      });
    } catch (error) {
      console.error('error: ', error);
    }
  }


  filter(value: any, index: any) {
    if (index === '1') {
      this.customers = this.getCustomerList(value);
    } else if (index === '2') {
      this.company = this.getCompanyList(value);
    }

  }


  async onchange(value: any, index: any) {
    try {
      if (index === '1') {
        this.getCustomerById(value);
      } else if (index === '2') {
        this.getCompanyById(value);
      }
    } catch (error) {
      console.error(error);
    }
  }


  async countryList() {
    try {
      this.authService.countryList().then(
        (res: any) => {
          this.Countries = res.data;
        },
        (error) => {
          console.error('error', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  async stateList(value: any) {
    try {
      this.authService.stateList(value).then(
        (res: any) => {
          this.State = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  async onChange(value: any) {
    try {
      this.stateList(value);
    } catch (error) {
      console.error(error);
    }
  }


  submit() {
    try {
      const selectedDate = this.quotationFormGroup.controls['startDate'].value;
      const formattedDate = this.datePipe.transform(selectedDate, 'yyyy-MM-dd HH:mm:ss');
      const selectedDate1 = this.quotationFormGroup.controls['endDate'].value;
      const formattedDate1 = this.datePipe.transform(selectedDate1, 'yyyy-MM-dd HH:mm:ss');
      const selectedDate2 = this.quotationFormGroup.controls['quotationExpiryDate'].value;
      const formattedDate2 = this.datePipe.transform(selectedDate2, 'yyyy-MM-dd HH:mm:ss');

      this.quotationFormGroup.value.startDate = formattedDate;
      this.quotationFormGroup.value.endDate = formattedDate1;
      this.quotationFormGroup.value.quotationExpiryDate = formattedDate2;
      this.authService.createQuotation(this.quotationFormGroup.value).then((res:any)=>{
        console.log('res: ', res);
      })
    } catch (error) {
      console.error('error: ', error);

    }
  }


  getCustomerList(data: any) {
    try {
      this.authService.getCustomrListFilter(data).then((res: any) => {
        this.customers = res.data;
      })
    } catch (error) {
      console.error('error: ', error);
    }
  }


  getCustomerById(id: any) {
    try {
      this.authService.getCustomerById(id).then((res: any) => {
        let response = res.data.customer;
        this.stateList(response.countryId);
        const cust = this.quotationFormGroup.value;
        Object.keys(cust).forEach((key) => {
          try {
            this.quotationFormGroup.controls[key].setValue(response[key]);
          } catch (error) {
            console.error('error: ', error);
          }
        });
      });
    } catch (error) {
      console.error('error: ', error);
    }
  }


  getCompanyList(data: any) {
    try {
      this.authService.getCompanyListFilter(data).then((res: any) => {
        this.company = res.data;
      })
    } catch (error) {
      console.error('error: ', error);
    }
  }


  getCompanyById(id: any) {
    try {
      this.authService.getCompanyById(id).then((res: any) => {
        let response = res.data.customer;
        this.stateList(response.company_countryId);
        const company = this.quotationFormGroup.value;
        Object.keys(company).forEach((key) => {
          try {
            this.quotationFormGroup.controls[key].setValue(response[key]);
          } catch (error) {
            console.error('error: ', error);
          }
        });
      });
    } catch (error) {
      console.error('error: ', error);
    }
  }
}
