import { Component, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/api/api.service';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-customize-web',
  templateUrl: './customize-web.component.html',
  styleUrls: ['./customize-web.component.css']
})
export class CustomizeWebComponent {



  userId: any;
  organization: any;
  editOrgForm: FormGroup;
  editorContent: any;
  image_name: any;
  LogoUrl: any;


  constructor(private location: Location, private authService: AuthService, private fb: FormBuilder) {
    this.editOrgForm = this.fb.group({
      address: ['', Validators.required],
      postalCode: ['', Validators.required],
      city: ['', Validators.required],
      countryId: ['', Validators.required],
      stateId: ['', Validators.required],
      prefferedCurrencies: ['', Validators.required],
      LogoUrl: [''],
      name: [''],
      pageContent_TnC: [''],
      pageContent_PrivacyPolicy: [''],
      ThemeColor: [''],
      userId: [''],
    });
  }

  ngOnInit(): void {
    // this.token();
  }

  uploadFiles(files: File[]) {
    for (const file of files) {
    }
  }

  quillConfig = {
    toolbar: {
      container: [
        ['bold', 'italic', 'underline', 'strike'],
        ['code-block', 'blockquote'],
        [{ list: 'ordered' }, { list: 'bullet' }],
        [{ header: [1, 2, 3, 4, 5, 6, false] }],
        [{ color: [] }, { background: [] }],
        ['link', 'image'],
        ['clean'],

      ],
    },
  }


  back() {
    this.location.back();
  }

  token() {
    let data: any = localStorage.getItem('user');
    if (data) {
      data = JSON.parse(data);
      this.userId = data.user.id;
      this.getOrganization(this.userId);
    }
  }


  getOrganization(userId: any) {
    try {
      this.authService.getOrgByUserId(userId).then((res: any) => {
        let response = res.data[0];
        const userFromValue = this.editOrgForm.value;
        Object.keys(userFromValue).forEach((key) => {
          try {
            if (key === 'LogoUrl') {
              this.LogoUrl = response.LogoUrl;
              let data = response.LogoUrl;
              this.displaySingleImg(data);
            }
            else {
              this.editOrgForm.controls[key].setValue(response[key]);
            }
          } catch (error) {
            console.error('error: ', error);
          }
        })
      }, (error) => {
        console.error('error: ', error);

      })
    } catch (error) {
      console.error('error: ', error);

    }
  }

  selectColor(color: any) {
    this.editOrgForm.get('ThemeColor')?.setValue(color);
  }


  displaySingleImg(preview: any) {
    
  }

  onFileDropped(event: any) {
    // if (event && event.length > 0) {
    //   const file = event[0];
    //   this.authService.uploadImage(file, '').then((res: any) => {
    //     this.image_name = res.data.data[0].name;
    //     let data = document.querySelectorAll('ngx-file-drag-drop');
    //     let imgR = document.getElementsByClassName('timepageimg0');
    //     if (imgR.length) {
    //       data[0].removeChild(imgR[0]);
    //     }
    //     this.LogoUrl = this.image_name;
    //     this.displaySingleImg(this.LogoUrl);
    //     this.editOrgForm.value.LogoUrl = this.LogoUrl;

    //   })

    // }
  }

  submit() {
    try {
      this.editOrgForm.value.LogoUrl = this.LogoUrl;
      this.authService.updateOrganization(this.editOrgForm.value, this.userId).then((res: any) => {

      }, (error) => {
        console.error('error: ', error);

      })
    } catch (error) {
      console.error('error: ', error);

    }
  }

}
