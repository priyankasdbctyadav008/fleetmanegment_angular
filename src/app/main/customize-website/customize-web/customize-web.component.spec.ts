import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizeWebComponent } from './customize-web.component';

describe('CustomizeWebComponent', () => {
  let component: CustomizeWebComponent;
  let fixture: ComponentFixture<CustomizeWebComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomizeWebComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomizeWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
