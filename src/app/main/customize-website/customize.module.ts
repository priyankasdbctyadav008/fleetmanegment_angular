import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomizeWebComponent } from './customize-web/customize-web.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AngularMaterialModule } from 'src/app/material.module';
import { QuillModule } from 'ngx-quill'


const routes: Routes = [
  {
    path: 'website',
    component: CustomizeWebComponent,
  },
  
];


@NgModule({
  declarations: [
    CustomizeWebComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    QuillModule.forRoot(),
    FlexLayoutModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    RouterModule.forChild(routes),

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomizeModule { }
