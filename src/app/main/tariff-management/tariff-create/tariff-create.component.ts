import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-tariff-create',
  templateUrl: './tariff-create.component.html',
  styleUrls: ['./tariff-create.component.css']
})
export class TariffCreateComponent {
  id: any
  myForm: FormGroup;
  vehicleType: any;
  vehicleCat: any
  constructor(private fb: FormBuilder, private authService: AuthService, private _snackBar: MatSnackBar, private route: ActivatedRoute) {
    this.myForm = this.fb.group({
      packageName: [''],
      packageDurationInDays: [''],
      vehiclesCategoryId: [''],
      vehicleTypeId: [''],
      mileageIncluded:[12],
      isUnlimitedMileage: [false],
      amount: [''],
      mileageUnit: ['Km'],
      isStartMonday: [false],
      isStartTuesday: [false],
      isStartWednesday: [false],
      isStartThursday: [false],
      isStartFriday: [false],
      isStartSaturday: [false],
      isStartSunday: [false],
    });
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    if (this.id) {
      this.getTariffById(this.id);
    }
    this.vehicle_type();
    this.vehicle_category();
  }

  vehicle_type() {
    try {
      this.authService.vehicleTypeList('').then((res: any) => {
        this.vehicleType = res.data;
        console.log('this.vehicleType: ', this.vehicleType);
      })
    } catch (error) {
      console.log(error);
    }
  }

  vehicle_category() {
    try {
      this.authService.vehicleCategoryList('').then((res: any) => {
        this.vehicleCat = res.data;
        console.log('this.vehicleCat: ', this.vehicleCat);
      })
    } catch (error) {
      console.log(error);
    }
  }

  submit() {
    if (this.id) {
      this.updateTariff();
    } else {
      try {
        console.log('this.myForm.value: ', this.myForm.value);
        this.authService.tariffCreate(this.myForm.value).then((res: any) => {
          console.log('res: ', res);
        })
      } catch (error) {
        console.log(error);
      }
    }
  }

  getTariffById(id: any) {
    try {
      this.authService.getTariffById(id).then((res: any) => {
        console.log('res: ', res);
        let response = res.data.agencyTariffPackage;
        console.log('response: ', response);
        const userFromValue = this.myForm.value;
        console.log('userFromValue: ', userFromValue);
        Object.keys(userFromValue).forEach(key => {
          try {
            this.myForm.controls[key].setValue(response[key]);
          } catch (error) {
            console.log(' failed at ' + key + ' ' + error);
          }
        });
      })
    } catch (error) {
      console.log(error);
    }
  }

  updateTariff() {
    try {
      this.authService.updateTariff(this.id,this.myForm.value).then((res: any) => {
        console.log('res: ', res);
        // this.data = res.data;
      })
    } catch (error) {
      console.log(error);
    }
  }
}
