import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Component, ViewChild } from '@angular/core';
import { map } from 'rxjs';
import { AuthService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-tariff-management',
  templateUrl: './tariff-management.component.html',
  styleUrls: ['./tariff-management.component.css']
})
export class TariffManagementComponent {

  daysAvailbility: any = [{ "value": "isStartMonday", "isChecked": false}, { "value": "isStartTuesday", "isChecked": false }, { "value": "isStartWednesday", "isChecked": false }, { "value": "isStartThursday", "isChecked": false }, { "value": "isStartFriday", "isChecked": false }, { "value": "isStartSaturday", "isChecked": false }, { "value": "isStartSunday", "isChecked": false}];

  constructor(private breakpointObserver: BreakpointObserver,private authService: AuthService) {}
  

  public files: any[] = [];


  onUploadSuccess(file: any) {
    this.files.push(file);
  }

  removeFile(file: any) {
    const index = this.files.indexOf(file);
    if (index !== -1) {
      this.files.splice(index, 1);
    }
  }
  data: any
  ngOnInit() {
    this.tariffList();
  }

  tariffList(){
    try {
      this.authService.tariffList().then((res: any) => {
        this.data = res.data;
      })
    } catch (error) {
      console.log(error);
    }
  }

  deleteTariff(id:any) {
    try {
      this.authService.deleteTariff(id).then((res: any) => {
        this.tariffList();
        console.log('res: ', res);
        // this.data = res.data;
      })
    } catch (error) {
      console.log(error);
    }
  }
}
