import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TariffManagementComponent } from './tariff-management.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/material.module';
import { TariffCreateComponent } from './tariff-create/tariff-create.component';
import  { FlexLayoutModule } from '@angular/flex-layout'; 

const routes: Routes = [
  {
    path: 'create',
    component: TariffCreateComponent,
  },
  {
    path: 'list',
    component: TariffManagementComponent
  },
  {
    path: 'edit/:id',
    component: TariffCreateComponent
  }
];

@NgModule({
  declarations: [TariffManagementComponent, TariffCreateComponent],
  imports: [
    CommonModule,
    FormsModule,FlexLayoutModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    TariffManagementComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TariffModule { }
