import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { ConfirmPasswordValidator, characterOnlyValidator } from 'validator';
import { AuthService } from 'src/app/services/api/api.service';
import { Location } from '@angular/common';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';

@Component({
  selector: 'app-organization-create',
  templateUrl: './organization-create.component.html',
  styleUrls: ['./organization-create.component.css'],
  providers: [TranslatePipe]
})
export class OrganizationCreateComponent {
  createOrgForm: FormGroup;
  showPassword: any;
  password: any;
  Countries: any = [];
  State: any = [];
  Currency: any = [];
  id: any;
  selected: any;
  durationInSeconds = 1;
  jsonCurrency: any;
  city: any = [];
  viewId: any;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    public translate: TranslateService,
    private location: Location, public translatePipe: TranslatePipe
  ) {

    translate.setDefaultLang('en');

    this.createOrgForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])],
      phone: ['', [Validators.required]],
      name: ['', Validators.required],
      address: ['', Validators.required],
      postalCode: ['', Validators.required],
      city: ['', Validators.required],
      countryId: ['', Validators.required],
      stateId: ['', Validators.required],
      prefferedCurrencies: ['', Validators.required],
      role: ['2'],
      LogoUrl: ["null"],
      status: [false, Validators.required],
      userId: [''],
      themeColor: ["null"],
      pageContent_TnC: ["null"],
      pageContent_PrivacyPolicy: ["null"],
      password: ['', Validators.compose([Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")])],
      confirmPassword: ['', Validators.required],
      domain: [''],
    },
      {
        validators: ConfirmPasswordValidator('password', 'confirmPassword'),
      },
    );

  }

  ngOnInit() {
    this.countryList();
    this.currencyList();
    this.id = this.route.snapshot.params['id'];
    this.viewId = this.route.snapshot.params['viewId'];
    if (this.id) {
      this.organizationGetById(this.id);
    }else if(this.viewId){
      this.organizationGetById(this.viewId);
    }
  }


  back() {
    this.location.back();
  }


  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }


  public showVisibility() {
    this.showPassword = !this.showPassword;
  }


  showpassword() {
    this.password = !this.password;
  }

  onChangeCity(value: any) {
    try {
      this.cityList(value);
    } catch (error) {
      console.error(error);
    }
  }

  cityList(value: any) {
    try {
      this.authService.cityList(value).then(
        (res: any) => {
          this.city = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  async stateList(value: any) {
    try {
      this.authService.stateList(value).then(
        (res: any) => {
          this.State = res.data;
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  async countryList() {
    try {
      this.authService.countryList().then(
        (res: any) => {
          this.Countries = res.data;
        },
        (error) => {
          console.error('error', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  async onchange(value: any) {
    try {
      this.stateList(value);
    } catch (error) {
      console.error(error);
    }
  }



  async onChange(value: any) {
    this.Currency.map((curid: any) => {
      if (curid.id === value) {
        this.jsonCurrency = curid;
        this.createOrgForm.value.prefferedCurrencies = JSON.stringify(this.jsonCurrency);
      }
    });
  }


  async currencyList() {
    try {
      this.authService.currencyList().then(
        (res: any) => {
          this.Currency = res.data;
        },
        (error) => {

          console.log('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  submit() {
    if (this.id) {
      this.createOrgForm.controls['password'].clearValidators();
      this.createOrgForm.controls['confirmPassword'].removeValidators(Validators.required);
      this.createOrgForm.controls['password'].updateValueAndValidity();
      this.createOrgForm.controls['confirmPassword'].updateValueAndValidity();
      this.updateOrganization();

    } else {
      if (this.createOrgForm.valid) {
        try {
          let data = JSON.parse(JSON.stringify(this.createOrgForm.value));
          data.prefferedCurrencies = JSON.stringify(this.jsonCurrency);
          data.email = this.createOrgForm.value.email.toLowerCase();
          if (data.status) {
            data.status = '1';
          } else {
            data.status = '0';
          }
          this.authService.registerUser(data).then(
            (res: any) => {
              console.log('res.status: ', res);
              if (res.success === true) {
                localStorage.setItem('userName', res.user.first_name);
                this.openSnackBar(this.translatePipe.transform('CONFIRMATION_MASSAGE.ORG_CREATED'), 'true')
                this.router.navigate(['/organization/list']);
              }
              else if (res.success === 'fail') {
                if (res.message && typeof res.message === 'object') {
                  const errorMessage = Object.values(res.message)[0];
                  this.openSnackBar(errorMessage, 'false')
                }
              }
            },
            (error) => {
              console.log('error', error);
            }
          );
        } catch (error) {
          console.error(error);
        }
      }
    }
  }


  organizationGetById(id: any) {
    try {
      this.authService.organizationGetById(id).then(
        (res: any) => {
          let response = {

            ...res.data.organization,
            ...res.data.user
          }
          const userFromValue = this.createOrgForm.value;
          this.stateList(response.countryId);
          this.onChangeCity(response.stateId);
          Object.keys(userFromValue).forEach((key) => {
            try {
              if (key === 'prefferedCurrencies') {
                let prefferedcur = JSON.parse(response.prefferedCurrencies);
                let curid = prefferedcur.id;
                this.createOrgForm.controls[key].setValue(curid);
                this.onChange(curid);
              }
              else if (key === 'email') {
                this.createOrgForm.controls[key].setValue(response[key]);
                this.createOrgForm.controls[key].disable();
              }
              else {
                this.createOrgForm.controls[key].setValue(response[key]);
              }

            } catch (error) {
              console.error(' failed at ' + key + ' ' + error);
            }
          });
        },
        (error) => {
          console.error('error: ', error);
        }
      );
    } catch (error) {
      console.error(error);
    }
  }


  updateOrganization() {
    console.log(this.createOrgForm.value);
    if (this.createOrgForm.valid) {
      try {
        let data = JSON.parse(JSON.stringify(this.createOrgForm.value));
        data.prefferedCurrencies = JSON.stringify(this.jsonCurrency);

        if (data.status) {
          data.status = '1';
        } else {
          data.status = '0';
        }
        this.authService
          .updateOrgUser(data, this.id)
          .then((res: any) => {
            console.log('update', res);
            if (res.success === true) {
              this.openSnackBar(this.translatePipe.transform('CONFIRMATION_MASSAGE.ORG_UPDATE'), 'true')
              this.createOrgForm.reset();
              this.router.navigate(['/organization/list']);
            } else if (res.success === false) {
              if (res.message && typeof res.message === 'object') {
                const errorMessage = Object.values(res.message)[0];
                this.openSnackBar(errorMessage, 'false')
              }
            }
          }, (error: any) => {
            console.log('error: ', error);

          });
      } catch (error) {
        console.log('catch error: ', error);
      }
    }
  }
}
