import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { AuthService } from 'src/app/services/api/api.service';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationComponent } from 'src/app/helper/notification/notification.component';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDeleteComponent } from 'src/app/helper/confirmation-delete/confirmation-delete.component';
import { DialogBoxVehicleComponent } from 'src/app/helper/dialog-box-vehicle/dialog-box-vehicle.component';
@Component({
  selector: 'app-organization-list',
  templateUrl: './organization-list.component.html',
  styleUrls: ['./organization-list.component.css'],
  providers: [DatePipe, TranslatePipe]
})

export class OrganizationListComponent implements OnInit {
  @ViewChild('paginator') paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  organization: any = [];
  dataSource!: MatTableDataSource<any>;
  columnsToDisplay = ['name', 'email', 'created_at', 'actions']
  resultsLength = 10;
  durationInSeconds = 1;

  constructor(private authService: AuthService, public transalte: TranslateService, private _snackBar: MatSnackBar, private datePipe: DatePipe, public _translatePipe: TranslatePipe, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.getList();
  }

  openSnackBar(value: any, type: any) {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: this.durationInSeconds * 2000,
      data: {
        message: value,
        isSuccess: type
      },
      horizontalPosition: 'end',
      verticalPosition: 'top',
    })
  }

  openDialog(id: any, i: any, element: any) {
    const dialogRef = this.dialog.open(ConfirmationDeleteComponent, {
      data: {
        message: element,
        buttonText: {
          ok: 'Delete',
          cancel: 'Cancel'
        }
      }
    });

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.deleteOrganization(id, i)
      }
    });
  }


  async getList() {
    try {
      this.authService.listOrganization().then((res: any) => {
        this.organization = res.data;
        console.log('this.organization: ', this.organization);
        this.dataSource = new MatTableDataSource(this.organization);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        for (let i = 0; i <= this.organization.length; i++) {
          if (this.organization[i]) {
            let data = this.organization[i].created_at;
            const formattedDate = this.datePipe.transform(data, 'yyyy-MM-dd ');
            this.organization[i].created_at = formattedDate;
          }
        }
      });
    } catch (error) {
      console.error(error);
    }
  }


  async deleteOrganization(id: any, i: any) {
    try {
      this.authService.deleteOrg(id).then((res: any) => {
        console.log('res: ', res);
        if (res.success) {
          this.openSnackBar(res.message, 'true');
          this.getList();
        } else {
          this.openSnackBar(res.message, 'false');
          this.getList();
        }
      }, (error: any) => {
        this.openSnackBar(error.message, 'false');
      })
    } catch (error) {
      console.error(error);
    }
  }

  openDialogView(dialogData: any): void {
    const dialogRef = this.dialog.open(DialogBoxVehicleComponent, {
      data: dialogData,
      height: '607px',
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log('The dialog was closed');
    });
  }
}


