import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { OrganizationListComponent } from './organization-list.component';
import { AngularMaterialModule } from 'src/app/material.module';
import { OrganizationCreateComponent } from './organization-create/organization-create.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule, TranslateLoader, TranslatePipe } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { NgxMatIntlTelInputComponent } from 'ngx-mat-intl-tel-input';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}
const routes: Routes = [
  {
    path: 'list',
    component: OrganizationListComponent,
  },
  {
    path: 'create',
    component:OrganizationCreateComponent,
  },
  {
    path: 'edit/:id',
    component:OrganizationCreateComponent,
  },
  {
    path: 'view/:viewId',
    component:OrganizationCreateComponent,
  }
];

@NgModule({
  declarations: [
    OrganizationListComponent ,
    OrganizationCreateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    NgxMatIntlTelInputComponent,
    ReactiveFormsModule,
    AngularMaterialModule,
    RouterModule.forChild(routes),
    TranslateModule,
  ],
 
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrganizationModule { }
