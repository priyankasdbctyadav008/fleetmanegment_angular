import { AfterViewInit, Component } from '@angular/core';
import { AuthService } from './services/api/api.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from './services/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements AfterViewInit{

  isLoading: any = false;

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute, public httpService: HttpService) {
   
  }

  ngAfterViewInit() {
    // Update the boolean property here
    this.httpService.isLoading.subscribe((value) => {
      this.isLoading = value;
     })
  }
}


