import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularMaterialModule } from './material.module';
import { AuthModule } from './layouts/auth-layout/auth.module';
import { SiteLayoutModule } from './layouts/app-layout/site-layout.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardsModule } from './main/dashboard/dashboard.module';
import { OrganizationModule } from './main/organization-management/organization.module';
import { CookieService } from 'ngx-cookie-service';
import { AppHttpInterceptor } from './services/interceptors/app.http.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './auth.guard';
import { ToastrModule } from 'ngx-toastr';
import { NgxMatIntlTelInputComponent } from 'ngx-mat-intl-tel-input';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    AngularMaterialModule,
    AuthModule,
    DashboardsModule,
    OrganizationModule,
    FormsModule,
    NgxMatIntlTelInputComponent,
    SiteLayoutModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    CookieService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppHttpInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
