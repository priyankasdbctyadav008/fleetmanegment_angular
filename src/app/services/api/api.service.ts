import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { HttpService } from '../http.service';
import { CookieService } from 'ngx-cookie-service';
import { AnimationFrameScheduler } from 'rxjs/internal/scheduler/AnimationFrameScheduler';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private router: Router,
    private httpService: HttpService,
    private cookiesService: CookieService,
    private http: HttpClient

  ) { }

  loginUser(user: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('login', user).subscribe({
          next: (res: any) => {
            if (res && res.success === true) {
              localStorage.setItem('user', JSON.stringify(res));
              this.cookiesService.set('user', JSON.stringify(res));
            }
            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            this.httpService.isLoading.next(false);
            reject(error);
          },
        });
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);
      }
    });
  }


  token() {
    let data: any = localStorage.getItem('user');
    if (data) {
      data = JSON.parse(data);
      return data.authorisation?.token;
    }
  }

  isLoggedIn() {
    if (this.token()) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('ActiveLink');
    this.router.navigate(['/auth/signin']);
  }

  async registerUser(user: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('register', user).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
            reject(error);
          },
        });
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);
      }
    });
  }

  async forgotPassowrd(email: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('forgot_password', email).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            this.httpService.isLoading.next(false);
            reject(error);
          },
        });
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);
      }
    });
  }

  async currencyList(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get('list_currency', '').subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  async countryList(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.get('country', '').subscribe({
          next: (res) => {
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  async stateList(value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`states/${value}`, '').subscribe({
          next: (res) => {
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  async cityList(value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`city/${value}`, '').subscribe({
          next: (res) => {
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  async sendOtp(value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post(`resendOtp?email=${value}`, '').subscribe({
          next: (res) => {
            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            this.httpService.isLoading.next(false);
            reject(error);
          },
        });
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);
      }
    });
  }

  async verifyEmailOtp(email: any, value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        let data = { email: email, otp: value };
        await this.httpService.post('verifyOtp', data).subscribe({
          next: (res) => {
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  //------------------------------------vehicle list----------------------------------------//
  async createVehicleAgency(user: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('create_agencyVehicle?sortBy=created_at&orderBy=desc', user).subscribe({
          next: (res) => {
            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            this.httpService.isLoading.next(false);
            reject(error);
          },
        });
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);
      }
    });
  }


  async vehicleList(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`list_agencyVehicle?searchKey=${data.searchKey}&searchVal=${data.searchVal}&sortBy=created_at&orderBy=desc`, data).subscribe({
          next: (res) => {
            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async vehicleListbyId(data: any, id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`vehicles_by_organizationId?organizationId=${id}&searchKey=${data.searchKey}&searchVal=${data.searchVal}&sortBy=created_at&orderBy=desc`, data).subscribe({
          next: (res) => {
            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  async deleteVehicle(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .delete(`delete_agencyVehicle/${id}`, '')
          .subscribe({
            next: (res) => {
              resolve(res);
              this.httpService.isLoading.next(false);
            },
            error: (error) => {
              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }



  async getVehicleById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`agencyVehicle/${id}`, '').subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);

          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    })
  }



  async uploadImage(files: any, type: any): Promise<any> {
    console.log('files: ', files);
    return new Promise(async (resolve, reject) => {
      try {
        const formData = new FormData();
        if (type == 'multi') {
          for (let i = 0; i < files.length; i++) {
            formData.append('files[]', files[i]);
          }
        } else {
          formData.append('files[]', files);
        }

        this.httpService.uploadImage('upload_vehicles_agency', formData)
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  }




  // async removeImage(files: any): Promise<any> {
  //   return new Promise(async (resolve, reject) => {
  //     try {
  //       const formData = new FormData();

  //         formData.append('Image', files);


  //       this.httpService.uploadImage('remove-image', formData)
  //         .then(response => {
  //           console.log('response: ', response);
  //           resolve(response);
  //         })
  //         .catch(error => {
  //           reject(error);
  //         });
  //     } catch (error) {
  //       reject(error);
  //     }
  //   });
  // }

  async removeImage(image: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('remove-image', { 'image': image }).subscribe({
          next: (response) => {
            console.log('response: ', response);
            resolve(response);
            this.httpService.isLoading.next(false);

          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    })
  }


  async vehicleUpdate(data: any, id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.httpService.isLoading.next(true);
      try {
        await this.httpService.post(`update_agencyVhicle/${id}`, data).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          }, error: (error) => {
            reject(error);
          }
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);

      }
    })
  }





  ///---------------------------------------AGENCY API------------------------------------//
  async agencyList(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.httpService.isLoading.next(true);
      try {
        await this.httpService.get('list_agency?sortBy=created_at&orderBy=desc', '').subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  async agencyListById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.httpService.isLoading.next(true);
      try {
        await this.httpService.get(`agency_by_organizationId/${id}?sortBy=created_at&orderBy=desc`, '').subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  async agencyDropDown(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.httpService.isLoading.next(true);
      try {
        await this.httpService.get(`agency_dropDown/${id}`, '').subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }



  async agencyCreate(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('create_agency', data).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);

          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (e) {
        reject(e);
        this.httpService.isLoading.next(false);

      }
    });
  }

  async agencyUpdate(data: any, id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post(`update_agency/${id}`, data).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (e) {
        reject(e);
        this.httpService.isLoading.next(false);

      }
    });
  }

  async getAgencyById(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`agency/${data}`, data).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (e) {
        reject(e);

      }
    });
  }

  async deleteAgency(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.delete(`delete_agency/${id}`, '').subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);

          },
          error: (error) => {

            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);

      }
    });
  }
  //-------------------------------------Organization By UserId------------------------------//

  async getOrgByUserId(userId: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`get_user_organization/${userId}`, '').subscribe({
          next: (response) => {
            resolve(response);

          }, error: (error) => {

            reject(error);

          }
        })
      } catch (error) {
        reject(error);
      }
    })
  }
  //--------------------------------ORGANIZATION----------------------------------------//

  async listOrganization(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get('list_organizations?sortBy=created_at&orderBy=desc', '').subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);

          },
          error: (error) => {

            this.httpService.isLoading.next(false);
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);

      }
    });
  }
  async createOrganization(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('create_organization', data).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);

          },
          error: (error) => {

            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);

      }
    });
  }

  async organizationGetById(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`organization/${data}`, '').subscribe({
          next: (response) => {
            resolve(response);

          },
          error: (error) => {

            reject(error);
          },
        });
      } catch (error) {
        reject(error);

      }
    });
  }

  async updateOrganization(data: any, id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .post(`update_organization/${id}`, data)
          .subscribe({
            next: (response) => {
              resolve(response);
              this.httpService.isLoading.next(false);

            },
            error: (error) => {
              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });

  }


  async updateOrgUser(data: any, id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post(`update_user_org/${id}`, data).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);


          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          }
        })

      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    })
  }


  async deleteOrg(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .delete(`delete_organization/${id}`, '')
          .subscribe({
            next: (response) => {
              resolve(response);
              this.httpService.isLoading.next(false);

            },
            error: (error) => {
              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {

        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }
  //-------------------------------TARIFF-API----------------------------------//

  async tariffList(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`list_agencyTariffPackage?sortBy=created_at&orderBy=desc`, '').subscribe({
          next: (res) => {

            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async getTariffById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`agencyTariffPackage/${id}`, '').subscribe({
          next: (res) => {
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }


  async updateTariff(id: any, data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .post(`update_agencyTariffPackage/${id}`, data)
          .subscribe({
            next: (response) => {
              resolve(response);
              this.httpService.isLoading.next(false);
            },
            error: (error) => {

              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);

      }
    });
  }


  async tariffCreate(value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .post(`create_agencyTariffPackage`, value)
          .subscribe({
            next: (res) => {

              resolve(res);
              this.httpService.isLoading.next(false);
            },
            error: (error) => {
              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async deleteTariff(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .delete(`delete_agencyTariffPackage/${id}`, '')
          .subscribe({
            next: (response) => {
              resolve(response);
              this.httpService.isLoading.next(false);

            },
            error: (error) => {

              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {

        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  //---------------------------------VEHICLE TYPE  -----------------------------------------------//
  async vehicleTypeList(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`list_vehiclesType?&searchKey=${data.searchKey}&searchVal=${data.searchVal}&sortBy=created_at&orderBy=desc`, '').subscribe({
          next: (res) => {

            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async deleteVehicleType(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .delete(`delete_vehiclesType/${id}`, '')
          .subscribe({
            next: (res) => {

              resolve(res);
              this.httpService.isLoading.next(false);
            },
            error: (error) => {
              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async createVehicleType(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post(`create_vehiclesType`, data).subscribe({
          next: (res) => {
            resolve(res);
            this.httpService.isLoading.next(false);

          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async updateVehicleType(id: any, data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .post(`update_vehiclesType/${id}`, data)
          .subscribe({
            next: (res) => {

              resolve(res);
              this.httpService.isLoading.next(false);
            },
            error: (error) => {

              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async getVehicleTypeById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`vehiclesType/${id}`, '').subscribe({
          next: (res) => {
            resolve(res);

          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }


  async getTypeDropDown(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`dropDown_vehiclesType/${id}`, '').subscribe({
          next: (res) => {
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  //------------------------------VEHICLE CATEGORY-------------------------------------------//
  async vehicleCategoryList(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`list_vehiclesCategory?&searchKey=${data.searchKey}&searchVal=${data.searchVal}&sortBy=created_at&orderBy=desc`, '').subscribe({
          next: (res) => {
            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async createVehicleCategory(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post(`create_vehiclesCategory`, data).subscribe({
          next: (res) => {

            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async getCategoryDropDown(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`dropDown_vehiclesCategory`, '').subscribe({
          next: (res) => {

            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }


  async getCategoryById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`vehiclesCategory/${id}`, '').subscribe({
          next: (res) => {
            resolve(res);

          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }


  async updateVehicleCategory(id: any, data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .post(`update_vehiclesCategory/${id}`, data)
          .subscribe({
            next: (res) => {

              resolve(res);
              this.httpService.isLoading.next(false);
            },
            error: (error) => {
              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async deleteVehicleCategory(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .delete(`delete_vehiclesCategory/${id}`, '')
          .subscribe({
            next: (response) => {

              resolve(response);
              this.httpService.isLoading.next(false);
            },
            error: (error) => {
              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {

        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  //-----------------------------VEHICLE BRAND----------------------------------------//
  async vehicleBrandList(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`list_vehiclesBrand?&searchKey=${data.searchKey}&searchVal=${data.searchVal}&sortBy=created_at&orderBy=desc`, '').subscribe({
          next: (res) => {

            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {

        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  async vehicleBrandDropdown(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`dropDown_vehiclesBrand`, '').subscribe({
          next: (res) => {
            resolve(res);

          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {

        reject(error);
      }
    });
  }

  async getBrandById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`vehiclesBrand/${id}`, '').subscribe({
          next: (res) => {

            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  deleteVehicleBrand(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .delete(`delete_vehiclesBrand/${id}`, '')
          .subscribe({
            next: (res) => {
              resolve(res);
              this.httpService.isLoading.next(false);
            },
            error: (error) => {
              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  async createVehicleBrand(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post(`create_vehiclesBrand`, data).subscribe({
          next: (res) => {

            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  async updateVehicleBrand(id: any, data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .post(`update_vehiclesBrand/${id}`, data)
          .subscribe({
            next: (res) => {
              resolve(res);
              this.httpService.isLoading.next(false);

            },
            error: (error) => {
              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  //------------------------------------------User api------------------------------------//

  async getUserById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`user/${id}`, '').subscribe({
          next: (res) => {
            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  async getUserByEmail(email: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`user_by_email/${email}`, '').subscribe({
          next: (res) => {

            resolve(res);
            this.httpService.isLoading.next(false);
          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }

  async userUpdate(data: any, id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post(`update_user/${id}`, data).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (e) {
        reject(e);
        this.httpService.isLoading.next(false);

      }
    });
  }



  //-----------------------------------------Customer-Quotation-----------------------------------------------------//

  async createQuotation(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post('create_customerQuotation', data).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);

          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          }
        })
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    })
  }


  getQuotationList(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`list_customerQuotation?searchKey=${data.searchKey}&searchVal=${data.searchVal}&sortBy=created_at&orderBy=desc`, data).subscribe({
          next: (response) => {
            resolve(response);

            this.httpService.isLoading.next(false);
          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          }

        })
      } catch (error) {
        this.httpService.isLoading.next(false);
        reject(error);
      }
    })
  }


  async getCustomQuote(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get('list_customerQuotation?sortBy=created_at&orderBy=desc', '').subscribe({
          next: (response) => {

            resolve(response);
            this.httpService.isLoading.next(false);

          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);

          }
        })
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);

      }
    })
  }

  // async getCustomrList(): Promise<any> {
  //   return new Promise(async (resolve, reject) => {
  //     try {
  //       this.httpService.isLoading.next(true);
  //       // await this.httpService.get(`list_customers?firstName=${data}`,data).subscribe({
  //         await this.httpService.get('list_customers','').subscribe({
  //         next: (response) => {
  //           console.log('response: ', response);
  //           resolve(response);
  //           this.httpService.isLoading.next(false); 

  //         }, error: (error) => {
  //           reject(error);
  //           this.httpService.isLoading.next(false); 

  //         }
  //       })
  //     } catch (error) {
  //       reject(error);
  //       this.httpService.isLoading.next(false); 

  //     }
  //   })
  // }


  async getCustomrListFilter(data: any): Promise<any> {
    console.log('data: ', data);
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`list_customers?firstName=${data}&sortBy=created_at&orderBy=desc`, data).subscribe({
          // await this.httpService.get('list_customers','').subscribe({
          next: (response) => {

            resolve(response);
            this.httpService.isLoading.next(false);

          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);

          }
        })
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);

      }
    })
  }


  getCustomerById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`customer/${id}`, '').subscribe({
          next: (response) => {

            resolve(response);
            this.httpService.isLoading.next(false);
          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          }
        })
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);

      }
    })
  }

  // =------------------------------------------company----------------------------------------//
  async getCompanyListFilter(data: any): Promise<any> {

    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`list_company?name=${data}&sortBy=created_at&orderBy=desc`, data).subscribe({
          // await this.httpService.get('list_customers','').subscribe({
          next: (response) => {

            resolve(response);
            this.httpService.isLoading.next(false);

          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);

          }
        })
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);

      }
    })
  }


  getCompanyById(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`company/${id}`, '').subscribe({
          next: (response) => {

            resolve(response);
            this.httpService.isLoading.next(false);
          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          }
        })
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);

      }
    })
  }
  //---------------------------------subscription-plan-------------------------------------------------//

  async getSubscriptionList(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get('list_subscriptionPlan?sortBy=created_at&orderBy=desc', '').subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          }
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    })
  }


  //---------------------------------reservation-settings-------------------------------//
  async reserveSettingCre(value: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService
          .post(`create_reservationSetting`, value)
          .subscribe({
            next: (res) => {

              resolve(res);
              this.httpService.isLoading.next(false);
            },
            error: (error) => {
              reject(error);
              this.httpService.isLoading.next(false);
            },
          });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }


  async getSettingById(userId: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`user_reservation_settings/${userId}`, '').subscribe({
          next: (response) => {
            resolve(response);

          }, error: (error) => {

            reject(error);

          }
        })
      } catch (error) {
        reject(error);
      }
    })
  }

  async updateReserveSetting(data: any, id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.post(`update_reservationSetting/${id}`, data).subscribe({
          next: (response) => {
            resolve(response);
            this.httpService.isLoading.next(false);
            console.log('response: ', response);

          }, error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          }
        })
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    })
  }


  async reserveSettingList(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.httpService.isLoading.next(true);
        await this.httpService.get(`list_reservationSetting?sortBy=created_at&orderBy=desc`, '').subscribe({
          next: (res) => {

            resolve(res);
            this.httpService.isLoading.next(false);
          },
          error: (error) => {
            reject(error);
            this.httpService.isLoading.next(false);
          },
        });
      } catch (error) {
        reject(error);
        this.httpService.isLoading.next(false);
      }
    });
  }
}