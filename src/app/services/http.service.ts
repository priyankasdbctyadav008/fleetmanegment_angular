import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, from } from 'rxjs';
import { catchError } from 'rxjs/operators';
import axios from 'axios';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private api = environment.apiUrl;
  private serverApi = environment.serverUrl;

  public isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor(private http: HttpClient) {

  }

  post(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.post(fullUrl, data)
  }

  get(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.get(fullUrl, data);
  }

  getC(url: string, data: any) {
    console.log('this.serverApi: ', this.serverApi,this.serverApi + url);
    const fullUrl = this.serverApi + url;
    return this.http.get(fullUrl, data, );
  }

  delete(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.delete(fullUrl, data);
  }

  put(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.put(fullUrl, data);
  }
  uploadImage(url: string, data: any) {
    console.log('data: ', data);
    let token: any = '';
    let user: any = localStorage.getItem('user');
    if (user) {
      user = JSON.parse(user);
      token = user.authorisation?.token;
    }
    return axios.post('http://192.168.0.60:8000/api/upload_vehicles_agency', data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': 'bearer ' + token
      }
    })
  }



}





