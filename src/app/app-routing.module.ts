import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { LoginGuard } from './login.guard';
import { PrivacyPolicyComponent } from './layouts/app-layout/privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from './layouts/app-layout/terms-condition/terms-condition.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'auth/signin'
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
    title: "Privacy-Fleet Management"
  },
  {
    path: 'terms-condition',
    component: TermsConditionComponent,
    title: "Terms-Fleet Management"
  },
  {
    path: 'auth',
    loadChildren: () => import('./layouts/auth-layout/auth.module').then(m => m.AuthModule),
    canActivate: [LoginGuard]
  },
  {
    path: '',
    loadChildren: () => import('./layouts/app-layout/site-layout.module').then(m => m.SiteLayoutModule),
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
