import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AngularMaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgToggleModule } from 'ng-toggle-button';
import { DialogBoxVehicleComponent } from './dialog-box-vehicle/dialog-box-vehicle.component';
import { NotificationComponent } from './notification/notification.component';
import { BrowserModule } from '@angular/platform-browser';
import { ConfirmationDeleteComponent } from './confirmation-delete/confirmation-delete.component';
import { ViewDetailsPopupComponent } from './view-details-popup/view-details-popup.component';


@NgModule({
  declarations: [DialogBoxVehicleComponent, NotificationComponent, ConfirmationDeleteComponent, ViewDetailsPopupComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,FlexLayoutModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelperModule { }
