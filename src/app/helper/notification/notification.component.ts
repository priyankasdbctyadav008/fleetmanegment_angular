import { Component, Inject, inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent {
  snackBarRef = inject(MatSnackBarRef);
 
  constructor(private _snackBar: MatSnackBar,@Inject(MAT_SNACK_BAR_DATA) public data: any) {
  
  }

}
