import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogBoxVehicleComponent } from './dialog-box-vehicle.component';

describe('DialogBoxVehicleComponent', () => {
  let component: DialogBoxVehicleComponent;
  let fixture: ComponentFixture<DialogBoxVehicleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogBoxVehicleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DialogBoxVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
