import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { VehicleCreateComponent } from 'src/app/main/vehicle-management/vehicle-create/vehicle-create.component';
import { AuthService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-dialog-box-vehicle',
  templateUrl: './dialog-box-vehicle.component.html',
  styleUrls: ['./dialog-box-vehicle.component.css']
})
export class DialogBoxVehicleComponent {

  vehicleBrand: any;
  responseObj: any;
  constructor(public dialogRef: MatDialogRef<VehicleCreateComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private authService: AuthService) { }

  ngOnInit(): void {
    this.brandDropDown();
    let element = document.querySelectorAll('.mat-mdc-dialog-container');
    element[0].classList.add("my-dialogBox-vehicle");
  }

  brandDropDown() {
    try {
      this.authService.vehicleBrandDropdown().then((res: any) => {
        this.vehicleBrand = res.data;
        console.log('this.data: ', this.data);
        this.compareValue(this.data);
      });
    } catch (error) {
      console.error('error: ', error);
    }
  }



  compareValue(data: any) {
    this.vehicleBrand.map((id: any) => {
      {
        if (data.vehicleBrandId === id.id) {
          data.vehicleBrandId = id.name
        }
      }
    });

    const jsonResponse = data.vehicleFeatures;
    const responseObj = JSON.parse(jsonResponse);
    data.Gps = responseObj.Gps;
    data.Bluetooth = responseObj.Bluetooth;
    data.Air_Conditioner = responseObj.Air_Conditioner;
    data.Reversing_Radar = responseObj.Reversing_Radar;
    data.isofix_Seat = responseObj.isofix_Seat;
    // const motor = data.motorization[0];
    // data.motorization = Object.keys(motor);
    const gearjson = JSON.parse(data.gearBox);
    data.gearBox = Object.keys(gearjson);
  }

}
