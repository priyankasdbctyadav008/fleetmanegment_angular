import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDetailsPopupComponent } from './view-details-popup.component';

describe('ViewDetailsPopupComponent', () => {
  let component: ViewDetailsPopupComponent;
  let fixture: ComponentFixture<ViewDetailsPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewDetailsPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewDetailsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
